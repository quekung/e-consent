<!DOCTYPE html>
<html lang="en">

<!-- Top Head -->
<?php include("incs/header-top.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-sub-hidden show-spinner">
    <?php include("incs/header.html") ?>
    <?php include("incs/sidebar-left.html") ?>

    <main>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="mb-2">
                        <h1>Consent Person</h1>
						<nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
							<ol class="breadcrumb pt-0">
								<!--<li class="breadcrumb-item">
									<a href="#">Home</a>
								</li>-->
								<li class="breadcrumb-item">
									<a href="#">Consent Person List</a>  | 
								</li>
								<li class="breadcrumb-item active text-gray" aria-current="page"> Personal Infomation</li>
							</ol>
						</nav>
                        <!--<div class="top-right-button-container">

                            <div class="btn-group">
                                <div class="btn btn-primary btn-lg pl-4 pr-0 check-button">
                                    <label class="custom-control custom-checkbox mb-0 d-inline-block">
                                        <input type="checkbox" class="custom-control-input" id="checkAll">
                                        <span class="custom-control-label">&nbsp;</span>
                                    </label>
                                </div>
                                <button type="button"
                                    class="btn btn-lg btn-primary dropdown-toggle dropdown-toggle-split"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a class="dropdown-item" href="#">Action</a>
                                    <a class="dropdown-item" href="#">Another action</a>
                                </div>
                            </div>
                        </div>-->
                    </div>



                    <div class="separator mb-5"></div>
					<!-- Topzone -->
					<div class="top-zone">
						<h2 class="sub-head font-weight-bold text-medium mb-3">Consent Person</h2>
						<div class="row">
								 <div class="col-12 col-lg-8 mb-4">

                                    <div class="card question d-flex h-100 edit-quesiton">
                                                <div class="card-title pt-4 pl-4 pr-4 mb-0 d-flex justify-content-between min-width-zero">

                                                    <div class="align-self-center d-flex flex-column flex-md-row justify-content-between min-width-zero align-items-md-center">
                                                       <h5 class=" font-weight-bold">Master Information</h5>
                                                    </div>
                                                    <!--<div class="custom-control custom-checkbox pl-1 align-self-center">
                                                      
                                                        <button class="btn btn-outline-theme-3 icon-button rotate-icon-click rotate" type="button" data-toggle="collapse" data-target="#q1" aria-expanded="true" aria-controls="q1">
                                                            <i class="simple-icon-arrow-down with-rotate-icon"></i>
                                                        </button>
                                                    </div>-->
                                                </div>
                                                <div class="question-collapse collapse show">
                                                    <div class="card-body row">
                                     					<div class="col-sm-4 d-flex justify-content-center justify-content-xl-start">
															<div class="user-avatar">
															<figure><img src="img/profile-pic-l-2.jpg" class="img-thumbnail rounded-2"></figure>
															<div class="mt-4 text-center">
																<a href="javascript:;" title="Edit Information" class="btn btn-outline-primary btn-block">Edit Information</a>
															</div>
															</div>
														</div>
														<div class="user-info col-sm-8">
															<div class="row mb-0">
																<div class="col-sm-6">
																	<p class="text-muted text-small mb-1">Name Lastname (thai) :</p>
																	<p>
																		นาย เอทต้า เกย์กลอลี่
																	</p>
																</div>
																<div class="col-sm-6">
																	<p class="text-muted text-small mb-1">Name Lastname (English) :</p>
																	<p>
																		Mr. Etta Gregory
																	</p>
																</div>
															</div>
															
															<div class="row mb-0">
																<div class="col-sm-6">
																	<p class="text-muted text-small mb-1">Gender :</p>
																	<p>
																		Male
																	</p>
																</div>
																<div class="col-sm-6">
																	<p class="text-muted text-small mb-1">Birth Date :</p>
																	<p>
																		15-09-1984
																	</p>
																</div>
															</div>
															
															<div class="row mb-0">
																<div class="col-sm-6">
																	<p class="text-muted text-small mb-1">ID Card :</p>
																	<p>
																		98056999087334
																	</p>
																</div>
																<div class="col-sm-6">
																	<p class="text-muted text-small mb-1">Phone Number :</p>
																	<p>
																		0984456678
																	</p>
																</div>
															</div>
															
															<div class="row mb-0">
																<div class="col-sm-6">
																	<p class="text-muted text-small mb-1">Email :</p>
																	<p>
																		sarun.s@gmail.com
																	</p>
																</div>
																<div class="col-sm-6">
																	&nbsp;
																</div>
															</div>
															
															<div class="separator mb-3"></div>
															
															<div class="row mb-0">
																<div class="col-sm-6">
																	<p class="text-muted text-small mb-1">Address Line1 :</p>
																	<p>
																		437/109 ลุมพินีคอนโด  ถนนรัตนาธิเบศร์
																	</p>
																</div>
																<div class="col-sm-6">
																	<p class="text-muted text-small mb-1">Address Line2 :</p>
																	<p>
																		-
																	</p>
																</div>
															</div>
															
															<div class="row mb-0">
																<div class="col-sm-6">
																	<p class="text-muted text-small mb-1">Sub District :</p>
																	<p>
																		บางกระสอ
																	</p>
																</div>
																<div class="col-sm-6">
																	<p class="text-muted text-small mb-1">District :</p>
																	<p>
																		เมืองนนทบุรี
																	</p>
																</div>
															</div>
															
															<div class="row mb-0">
																<div class="col-sm-6">
																	<p class="text-muted text-small mb-1">Province :</p>
																	<p>
																		นนทบุรี
																	</p>
																</div>
																<div class="col-sm-6">
																	<p class="text-muted text-small mb-1">Zipcode :</p>
																	<p>
																		101110
																	</p>
																</div>
															</div>
														
														</div>

                                                        

                                                    </div>
                                                </div>
                                            </div>
                                </div>

                                <div class="col-lg-4 col-12 mb-4">
                                    <div class="card h-100">
                                        <!--<div class="position-absolute card-top-buttons">
                                            <button class="btn btn-header-light icon-button">
                                                <i class="simple-icon-pencil"></i>
                                            </button>
                                        </div>-->
										<h5 class="card-title font-weight-bold border-bottom p-4 mb-0">Overview</h5>
										<div class="separator"></div>
                                        <div class="card-body">
      
                                            <ul class="list-unstyled mb-0">
												<li class="mb-2"><a href="javascript:;" data-toggle="modal" data-backdrop="static" data-target="#overView" class="btn-link">ID Card (1)</a></li>
												<li class="mb-2"><a href="javascript:;" data-toggle="modal" data-backdrop="static" data-target="#overView" class="btn-link">Phone Number (3)</a></li>
												<li class="mb-2"><a href="javascript:;" data-toggle="modal" data-backdrop="static" data-target="#overView" class="btn-link">Email (2)</a></li>
												<li class="mb-2"><a href="javascript:;" data-toggle="modal" data-backdrop="static" data-target="#overView" class="btn-link">Address (3)</a></li>
												<li class="mb-2"><a href="javascript:;" data-toggle="modal" data-backdrop="static" data-target="#overView" class="btn-link">Consent Contract ID Card (2)</a></li>
												<li class="mb-2"><a href="javascript:;" data-toggle="modal" data-backdrop="static" data-target="#overView" class="btn-link">Consent Contract Phone Number (2)</a></li>
												<li class="mb-2"><a href="javascript:;" data-toggle="modal" data-backdrop="static" data-target="#overView" class="btn-link">Consent Contract Email (2)</a></li>
												<li class="mb-2"><a href="javascript:;" data-toggle="modal" data-backdrop="static" data-target="#overView" class="btn-link">Consent Contract Address (0)</a></li>
											</ul>
                                        </div>
                                    </div>
                                </div>

                               
                            </div>
					</div>
					
					<!-- Tabs-zone -->
					<div class="tabs-zone">
					<div class="head-tab-title">
						<ul class="nav nav-tabs separator-tabs ml-0 mb-3" role="tablist">
							<li class="nav-item">
								<a class="nav-link active" id="product-tab" data-toggle="tab" href="#Products" role="tab"
									aria-controls="first" aria-selected="true">Products (2)</a>
							</li>

							<li class="nav-item">
								<a class="nav-link" id="contract-tab" data-toggle="tab" href="#Contracts" role="tab"
									aria-controls="third" aria-selected="false">Contract (1)</a>
							</li>
						</ul>
						<div class="fix-r">
							<a class="col-12 btn btn-primary btn-sm" href="javascript:;"  data-toggle="modal" data-backdrop="static" data-target="#addConsent"> + Add Consent</a>
						</div>
					</div>
                    <div class="tab-content mb-4">
						<!-- tab1 -->
                        <div class="tab-pane show active" id="Products" role="tabpanel" aria-labelledby="products-tab">
							
						<div>
                                            <div class="card product1 d-flex mb-4 edit-product1">
                                                <div class="head-bar d-flex flex-grow-1 min-width-zero">
                                                    <div class="card-body align-self-center d-flex flex-column flex-md-row justify-content-between min-width-zero align-items-md-center">
                                                        <div class="list-item-heading mb-0 truncate w-80 mb-1 mt-1">
                                                           <b>Products Name : </b><span class="text-muted">Loan (1) </span>
                                                        </div>
                                                    </div>
                                                    <div class="custom-control custom-checkbox pl-1 head-bar align-self-center pr-4">
                                                        
                                                        <button class="btn btn-outline-theme-3 icon-button rotate-icon-click rotate" type="button" data-toggle="collapse" data-target="#p1" aria-expanded="false" aria-controls="p1">
                                                            <i class="simple-icon-arrow-down with-rotate-icon"></i>
                                                        </button>
                                                    </div>
                                                </div>

                                                <div class="product1-collapse collapse show" id="p1">
                                                    <div class="card-body pt-0 pl-4 pr-4">
                                                       <div class="bg-light p-2 mb-4 d-flex justify-content-between flex-nowrap align-items-center">
													   		<ul class="list-inline d-flex mb-0 justify-content-start">
																<li class="mr-4"><span class="text-muted">Information : </span><span>1</span></li>
																<li class="mr-4"><span class="text-muted">Product ID : </span><span>00345</span></li>
																<li class="mr-4"><span class="text-muted">Last Update : </span><span>19-05-2020  10:30</span></li>
																<li class="mr-4"><span class="text-muted">Status : </span><span class="text-primary">Active</span></li>
															</ul>
															<div class="ctrl-btn">
																<button class="btn btn-primary btn-sm" type="button">Edit </button>
																<button class="btn btn-outline-primary btn-sm" type="button">Delete </button>
															</div>
													   </div>
													   
													   <div class="detail-card">
													   		<div class="bg-white">
																<div class="row mb-0">
																	<div class="col-sm-4">
																		<p class="text-muted text-small mb-1">System Name :</p>
																		<p>
																			MBK Loan system management
																		</p>
																	</div>
																	<div class="col-sm-4">
																		<p class="text-muted text-small mb-1">Customer Type :</p>
																		<p>
																			Customer
																		</p>
																	</div>
																	<div class="col-sm-4">
																		<p class="text-muted text-small mb-1">Channel Input :</p>
																		<p>
																			Call API
																		</p>
																	</div>
																</div>
															</div>
															
															<div class="bg-white mb-4">
																<div class="row mb-0">
																	<div class="col-sm-4">
																		<p class="text-muted text-small mb-1">Cosent ID :</p>
																		<p>
																			1009654
																		</p>
																	</div>
																	<div class="col-sm-4">
																		<p class="text-muted text-small mb-1">Consent Category  :</p>
																		<p>
																			หนังสือให้ความยินยอมเปิดเผยข้อมูล
																		</p>
																	</div>
																	<div class="col-sm-4">
																		<p class="text-muted text-small mb-1">Cosent Sub Category :</p>
																		<p>
																			การให้ยินยอมให้เปิดเผยข้อมูลเฉพาะบางกรณีในกลุ่มธุรกิจการเงิน
																		</p>
																	</div>
																</div>
																<div class="row mb-0">
																	<div class="col">
																		<p class="text-muted text-small mb-1">Purpose of Consent :</p>
																		<p>
																			ตกลงยินยอมให้ บมจ.xxxx  เปิดผข้อมูลส่วนบุกกของช้พจ เช่น ชื่อ ช่องทางในการติดต่อเพศอายุการศึกษา เป็นต้น แต่ทั้งนี้ ไม่รวมถึงข้อมูลที่เกี่ยวกับบัญชีของข้พเจ้าเช่น เลขที่บัญชี ยอดคงเหลือในบัญชี รายการเคลื่อนไหวในบัญชีเป็นต้น ให้แก่กลุ่มธุรกิจทางการเงินของธนาคารและให้กลุ่มธุรกิจทางการเงินของธนาคารสามารถใช้ข้มูลดังกล่ว พื่อวัตถุประสงค์ทางการตลาด เช่น เพื่อวัตถุประสงค์ในการพิจารณานำเสนอผลิตภัณฑ์ หรือเพื่อส่งเสริมการขายผลิตภัณฑ์บริกร และข้อสนอพิเศษอื่นๆ ของบริษัทในกลุ่มธุรกิจการเงินของธนาคาร ให้แก่ข้าพเจ้า
																		</p>
																	</div>
																	
																</div>
																
																<div class="row mb-0">
																	<div class="col-sm-4">
																		<p class="text-muted text-small mb-1">Consent Start Date:</p>
																		<p>
																			19-05-2020
																		</p>
																	</div>
																	<div class="col-sm-4">
																		<p class="text-muted text-small mb-1">Consent Expire Date:</p>
																		<p>
																			19-05-2023
																		</p>
																	</div>
																	<div class="col-sm-4">
																		<p class="text-muted text-small mb-1">Consent Version:</p>
																		<p>
																			5.0 <a  href="javascript:;"  data-toggle="modal" data-backdrop="static" data-target="#History" class="text-primary">View version history </a>
																		</p>
																	</div>
																</div>
																<div class="row mb-0">
																	<div class="col-sm-4">
																		<p class="text-muted text-small mb-1">Other Document File  :</p>
																		<p class="mb-0">
																			<a href="#" class="text-primary">สำเนาบัตรประชาชน.pdf</a><i class="glyph-icon iconsminds-close"></i>
																		</p>
																		<p class="mb-0">
																			<a href="#" class="text-primary">สำเนาทะเบียนบ้าน.pdf</a><i class="glyph-icon iconsminds-close"></i>
																		</p>
																		<p class="mb-0">
																			<a href="#" class="text-primary">สำเนาบุ๊คแบงค์ KTB.pdf</a><i class="glyph-icon iconsminds-close"></i>
																		</p>
																	</div>
																	<div class="col-sm-4">
																		<p class="text-muted text-small mb-1">Consent file :</p>
																		<p>
																			<a href="#" class="text-primary">MBK_2020_loan_agreement.pdf</a>
																		</p>
																		<p><button class="btn btn-outline-primary btn-sm" type="button">Resend  E-mail </button></p>
																	</div>
																	<div class="col-sm-4">
																		&nbsp;
																	</div>
																</div>
																
															</div>
															
															<div class="bg-white">
																																
																<div class="user-info-inner">
																	<div class="row mb-0">
																		<div class="col-sm-4">
																			<p class="text-muted text-small mb-1">Name Lastname (thai) :</p>
																			<p>
																				นาย เอทต้า เกย์กลอลี่
																			</p>
																		</div>
																		<div class="col-sm-4">
																			<p class="text-muted text-small mb-1">Name Lastname (English) :</p>
																			<p>
																				Mr. Etta Gregory
																			</p>
																		</div>
																		<div class="col-sm-4">
																			<p class="text-muted text-small mb-1">Gender :</p>
																			<p>
																				Male
																			</p>
																		</div>
																	</div>

																	<div class="row mb-0">
																		
																		<div class="col-sm-4">
																			<p class="text-muted text-small mb-1">Birth Date :</p>
																			<p>
																				15-09-1984
																			</p>
																		</div>
														
																		<div class="col-sm-4">
																			<p class="text-muted text-small mb-1">ID Card :</p>
																			<p>
																				98056999087334
																			</p>
																		</div>
																		<div class="col-sm-4">
																			<p class="text-muted text-small mb-1">Phone Number :</p>
																			<p>
																				0984456678
																			</p>
																		</div>
																	</div>

																	<div class="row mb-0">
																		<div class="col">
																			<p class="text-muted text-small mb-1">Email :</p>
																			<p>
																				sarun.s@gmail.com
																			</p>
																		</div>
																		
																	</div>
																	
																	<div class="separator mb-3"></div>

																	
																	<div class="row mb-0">
																		<div class="col-sm-4">
																			<p class="text-muted text-small mb-1">Address Line1 :</p>
																			<p>
																				437/109 ลุมพินีคอนโด  ถนนรัตนาธิเบศร์
																			</p>
																		</div>
																		<div class="col-sm-4">
																			<p class="text-muted text-small mb-1">Address Line2 :</p>
																			<p>
																				-
																			</p>
																		</div>
																		<div class="col-sm-4">
																			<p class="text-muted text-small mb-1">Sub District :</p>
																			<p>
																				บางกระสอ
																			</p>
																		</div>
																	</div>

																	<div class="row mb-0">
																		
																		<div class="col-sm-4">
																			<p class="text-muted text-small mb-1">District :</p>
																			<p>
																				เมืองนนทบุรี
																			</p>
																		</div>
																	
																		<div class="col-sm-4">
																			<p class="text-muted text-small mb-1">Province :</p>
																			<p>
																				นนทบุรี
																			</p>
																		</div>
																		<div class="col-sm-4">
																			<p class="text-muted text-small mb-1">Zipcode :</p>
																			<p>
																				101110
																			</p>
																		</div>
																	</div>

																</div>
															</div>
															
													   </div>
													   
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

								<div>
									<div class="card product2 d-flex mb-4 edit-product2">
										<div class="d-flex flex-grow-1 min-width-zero">
											<div class="card-body align-self-center d-flex flex-column flex-md-row justify-content-between min-width-zero align-items-md-center">
												<div class="list-item-heading mb-0 truncate w-80 mb-1 mt-1">
												   <b>Products Name : </b><span class="text-muted">Life Insurance (1) </span>
												</div>
											</div>
											<div class="custom-control custom-checkbox pl-1 head-bar align-self-center pr-4">

												<button class="btn btn-outline-theme-3 icon-button rotate-icon-click rotate" type="button" data-toggle="collapse" data-target="#p2" aria-expanded="false" aria-controls="p2">
													<i class="simple-icon-arrow-down with-rotate-icon"></i>
												</button>
											</div>
										</div>

										<div class="product2-collapse collapse show" id="p2">
											<div class="card-body pt-0 pl-4 pr-4">
											   <div class="bg-light p-2 mb-4 d-flex justify-content-between flex-nowrap align-items-center">
													<ul class="list-inline d-flex mb-0 justify-content-start">
														<li class="mr-4"><span class="text-muted">Information : </span><span>1</span></li>
														<li class="mr-4"><span class="text-muted">Product ID : </span><span>00345</span></li>
														<li class="mr-4"><span class="text-muted">Last Update : </span><span>19-05-2020  10:30</span></li>
														<li class="mr-4"><span class="text-muted">Status : </span><span class="text-primary">Active</span></li>
													</ul>
													<div class="ctrl-btn">
														<button class="btn btn-primary btn-sm" type="button">Edit </button>
														<button class="btn btn-outline-primary btn-sm" type="button">Delete </button>
													</div>
											   </div>

											   <div class="detail-card">
													<div class="bg-white">
														<div class="row mb-0">
															<div class="col-sm-4">
																<p class="text-muted text-small mb-1">System Name :</p>
																<p>
																	www.ngerntidlor.com
																</p>
															</div>
															<div class="col-sm-4">
																<p class="text-muted text-small mb-1">Customer Type :</p>
																<p>
																	Customer
																</p>
															</div>
															<div class="col-sm-4">
																<p class="text-muted text-small mb-1">Channel Input :</p>
																<p>
																	Import File / Upload File
																</p>
															</div>
														</div>
													</div>

													<div class="bg-light mx-n4 p-4 mb-4">
														<div class="row mb-0">
															<div class="col-sm-4">
																<p class="text-muted text-small mb-1">Cosent ID :</p>
																<p>
																	2005334
																</p>
															</div>
															<div class="col-sm-4">
																<p class="text-muted text-small mb-1">Consent Category  :</p>
																<p>
																	สัญญาเงินกู้ตามกรมธรรม์
																</p>
															</div>
															<div class="col-sm-4">
																<p class="text-muted text-small mb-1">Cosent Sub Category :</p>
																<p>
																	การยินยอมขอรับเงินกู้ตามกรมธรรม์หรือ<br>ผลประโยชน์ตามกรมธรรมผ่านบัญชี
																</p>
															</div>
														</div>
														<div class="row mb-0">
															<div class="col">
																<p class="text-muted text-small mb-1">Purpose of Consent :</p>
																<p>
																	บัญชีพร้อมเพย์ ที่สมัครโดยใช้เลขที่บัตรประชาชนของผู้เอาประกันภัยเลขที่  00122345  (แนบสำเนาบัตรประชาชน) 
																</p>
															</div>

														</div>

														<div class="row mb-0">
															<div class="col-sm-4">
																<p class="text-muted text-small mb-1">Consent Start Date:</p>
																<p>
																	19-05-2020
																</p>
															</div>
															<div class="col-sm-4">
																<p class="text-muted text-small mb-1">Consent Expire Date:</p>
																<p>
																	19-05-2023
																</p>
															</div>
															<div class="col-sm-4">
																<p class="text-muted text-small mb-1">Consent Version:</p>
																<p>
																	1.0 <a  href="javascript:;"  data-toggle="modal" data-backdrop="static" data-target="#History" class="text-primary">View version history </a>
																</p>
															</div>
														</div>
														<div class="row mb-0">
															<div class="col-sm-4">
																<p class="text-muted text-small mb-1">Other Document File  :</p>
																<p>
																	<a href="#" class="text-primary">สำเนาบัตรประชาชน.pdf</a><i class="glyph-icon iconsminds-close"></i>
																</p>
															</div>
															<div class="col-sm-4">
																<p class="text-muted text-small mb-1">Consent file :</p>
																<p>
																	<a href="#" class="text-primary">tidlor_2020_loan_agreement.pdf</a>
																</p>
																<p><button class="btn btn-outline-primary btn-sm" type="button">Resend  E-mail </button></p>
															</div>
															<div class="col-sm-4">
																&nbsp;
															</div>
														</div>
														<div class="separator mb-3"></div>
														<div class="row mb-0">
															<div class="col-sm-4">
																<p class="text-muted text-small mb-1">Cosent ID :</p>
																<p>
																	2005335
																</p>
															</div>
															<div class="col-sm-4">
																<p class="text-muted text-small mb-1">Consent Category  :</p>
																<p>
																	หนังสือยินยอมชำระเบี้ยประกันผ่านบัญชีธนาคาร
อัติโนมัติ
																</p>
															</div>
															<div class="col-sm-4">
																<p class="text-muted text-small mb-1">Cosent Sub Category :</p>
																<p>
																	การยินยอมให้ชำระเบี้ยประกันผ่านบัญชีธนาคาร <br>โดยอัติโนมัติ 
																</p>
															</div>
														</div>
														<div class="row mb-0">
															<div class="col">
																<p class="text-muted text-small mb-1">Purpose of Consent :</p>
																<p class="mb-0">
																	ข้าพเจ้ายอมรับในเงื่อนไขดังนี้
																</p>
																<ol class="list-inline-item pl-3">
																	<li>ข้าพเจ้ามีความประสงค์ให้ เอไอเอ หักงินจากบัญชีงินฝากธนาคารข้างต้นเพื่อชำระเบี้ยประกันภัยของวดปัจุบัน รวมทั้งเบี้ยประกันภัยงวดต่อไปของกรมธรรม์ตามที่ เอไอเอ     
ได้พิจารณาออกให้ตามใบคำขอเอาประกันภัยเลขที่ดังกล่าวข้างต้น ทั้งนี้ จนกว่ากรมธรรดังกล่าวจะถูกบอกเลิกหรือสิ้นสุดลง</li>
<li>กรณีที่บัญชีงินฝากธนาคารที่ระบุ หรือเบี้ยประกันภัยได้เปลี่ยนแปลงไม่ว่าโดยสาเหตุใดก็ตาม หนังสือฉบับนี้ ยังคงมีผลบังคับใช้กับบัญชีธนาคารซึ่งเป็นบัญชีใหม่หรือเบี้ยประกันภัยที่ได้เปลี่ยนแปลงนั้นๆ  
ทุกประการ</li>
<li>ข้าพเจ้ายอมรับว่า หากธนาคารไม่สามารถหักบัญชีตามที่ระบุไว้ไม่ว่าโดยสาเหตุใดก็ตามให้ถือเสมือนหนึ่งว่ายังไม่มีการชำระเบี้ยประกันภัยของกรมรรม์ประกันภัยข้างต้น</li>
<li>กรมธรรที่ระบุข้างต้น จะต้องระบุชื่อเจ้าของบัญชี หรือบุคคลในครอบครัวของเจ้าของบัญชี เป็นผู้เอาประกันภัย เจ้าของกรมธรรม์และเหรือผู้ชำระเบี้ยประกันภัย</li>
<li>การหักบัญชีธนาคารดังกล่าวข้างต้นให้มีผลบังคับนับแต่วันที่เอไอเอทำการอนุมัติ และให้คงมีผลบังคับต่อไปจนกว่าข้าพเจ้าจะได้เพิกถอนโดยทำเป็นลายลักษณ์
อักษรส่งให้ธนาคารหรือเอไอเอทราบล่วงหน้าอย่างน้อย 30 วัน</li>
																</ol>
															</div>

														</div>

														<div class="row mb-0">
															<div class="col-sm-4">
																<p class="text-muted text-small mb-1">Consent Start Date:</p>
																<p>
																	19-05-2020
																</p>
															</div>
															<div class="col-sm-4">
																<p class="text-muted text-small mb-1">Consent Expire Date:</p>
																<p>
																	19-05-2023
																</p>
															</div>
															<div class="col-sm-4">
																<p class="text-muted text-small mb-1">Consent Version:</p>
																<p>
																	1.0 <a  href="javascript:;"  data-toggle="modal" data-backdrop="static" data-target="#History" class="text-primary">View version history </a>
																</p>
															</div>
														</div>
														<div class="row mb-0">
															<div class="col-sm-4">
																<p class="text-muted text-small mb-1">Other Document File  :</p>
																<p>
																	<a href="#" class="text-primary">สำเนาบัตรประชาชน.pdf</a><i class="glyph-icon iconsminds-close"></i>
																</p>
															</div>
															<div class="col-sm-4">
																<p class="text-muted text-small mb-1">Consent file :</p>
																<p>
																	<a href="#" class="text-primary">tidlor_2020_loan_agreement.pdf</a>
																</p>
																<p><button class="btn btn-outline-primary btn-sm" type="button">Resend  E-mail </button></p>
															</div>
															<div class="col-sm-4">
																&nbsp;
															</div>
														</div>
													</div>

													<div class="bg-white">

														<div class="user-info-inner">
															<div class="row mb-0">
																<div class="col-sm-4">
																	<p class="text-muted text-small mb-1">Name Lastname (thai) :</p>
																	<p>
																		นาย เอทต้า เกย์กลอลี่
																	</p>
																</div>
																<div class="col-sm-4">
																	<p class="text-muted text-small mb-1">Name Lastname (English) :</p>
																	<p>
																		Mr. Etta Gregory
																	</p>
																</div>
																<div class="col-sm-4">
																	<p class="text-muted text-small mb-1">Gender :</p>
																	<p>
																		Male
																	</p>
																</div>
															</div>

															<div class="row mb-0">

																<div class="col-sm-4">
																	<p class="text-muted text-small mb-1">Birth Date :</p>
																	<p>
																		15-09-1984
																	</p>
																</div>

																<div class="col-sm-4">
																	<p class="text-muted text-small mb-1">ID Card :</p>
																	<p>
																		98056999087334
																	</p>
																</div>
																<div class="col-sm-4">
																	<p class="text-muted text-small mb-1">Phone Number :</p>
																	<p>
																		0984456678
																	</p>
																</div>
															</div>

															<div class="row mb-0">
																<div class="col">
																	<p class="text-muted text-small mb-1">Email :</p>
																	<p>
																		sarun.s@gmail.com
																	</p>
																</div>

															</div>

															<div class="separator mb-3"></div>


															<div class="row mb-0">
																<div class="col-sm-4">
																	<p class="text-muted text-small mb-1">Address Line1 :</p>
																	<p>
																		437/109 ลุมพินีคอนโด  ถนนรัตนาธิเบศร์
																	</p>
																</div>
																<div class="col-sm-4">
																	<p class="text-muted text-small mb-1">Address Line2 :</p>
																	<p>
																		-
																	</p>
																</div>
																<div class="col-sm-4">
																	<p class="text-muted text-small mb-1">Sub District :</p>
																	<p>
																		บางกระสอ
																	</p>
																</div>
															</div>

															<div class="row mb-0">

																<div class="col-sm-4">
																	<p class="text-muted text-small mb-1">District :</p>
																	<p>
																		เมืองนนทบุรี
																	</p>
																</div>

																<div class="col-sm-4">
																	<p class="text-muted text-small mb-1">Province :</p>
																	<p>
																		นนทบุรี
																	</p>
																</div>
																<div class="col-sm-4">
																	<p class="text-muted text-small mb-1">Zipcode :</p>
																	<p>
																		101110
																	</p>
																</div>
															</div>

														</div>
													</div>

											   </div>

											</div>
										</div>


									</div>
								</div>

                                       
						</div>
						<!-- tab2 -->
						<div class="tab-pane fade" id="Contracts" role="tabpanel" aria-labelledby="contract-tab">
							<div>
									<div class="card product3 d-flex mb-4 edit-product3">
										<div class="d-flex flex-grow-1 min-width-zero">
											<div class="card-body align-self-center d-flex flex-column flex-md-row justify-content-between min-width-zero align-items-md-center">
												<div class="list-item-heading mb-0 truncate w-80 mb-1 mt-1">
												   <b>Contact  Name : </b><span class="text-muted">หนังสือให้ความยินยอมเปิดเผยข้อมูล (1) </span>
												</div>
											</div>
											<div class="custom-control custom-checkbox pl-1 head-bar align-self-center pr-4">

												<button class="btn btn-outline-theme-3 icon-button rotate-icon-click rotate" type="button" data-toggle="collapse" data-target="#p3" aria-expanded="false" aria-controls="p3">
													<i class="simple-icon-arrow-down with-rotate-icon"></i>
												</button>
											</div>
										</div>

										<div class="product2-collapse collapse show" id="p3">
											<div class="card-body pt-0 pl-4 pr-4">
											   <div class="bg-light p-2 mb-4 d-flex justify-content-between flex-nowrap align-items-center">
													<ul class="list-inline d-flex mb-0 justify-content-start">
														<li class="mr-4"><span class="text-muted">Information : </span><span>1</span></li>
														<li class="mr-4"><span class="text-muted">Product ID : </span><span>00345</span></li>
														<li class="mr-4"><span class="text-muted">Last Update : </span><span>19-05-2020  10:30</span></li>
														<li class="mr-4"><span class="text-muted">Status : </span><span class="text-primary">Active</span></li>
													</ul>
													<div class="ctrl-btn">
														<button class="btn btn-primary btn-sm" type="button">Edit </button>
														<button class="btn btn-outline-primary btn-sm" type="button">Delete </button>
													</div>
											   </div>

											   <div class="detail-card">
													<div class="bg-white">
														<div class="row mb-0">
															<div class="col-sm-4">
																<p class="text-muted text-small mb-1">System Name :</p>
																<p>
																	MBK Loan system management
																</p>
															</div>
															<div class="col-sm-4">
																<p class="text-muted text-small mb-1">Customer Type :</p>
																<p>
																	Customer
																</p>
															</div>
															<div class="col-sm-4">
																<p class="text-muted text-small mb-1">Channel Input :</p>
																<p>
																	Call API
																</p>
															</div>
														</div>
													</div>

													<div class="bg-light mx-n4 p-4 mb-4">
														<div class="row mb-0">
															<div class="col-sm-4">
																<p class="text-muted text-small mb-1">Cosent ID :</p>
																<p>
																	2005334
																</p>
															</div>
															<div class="col-sm-4">
																<p class="text-muted text-small mb-1">Consent Category  :</p>
																<p>
																	หนังสือให้ความยินยอมเปิดเผยข้อมูล
																</p>
															</div>
															<div class="col-sm-4">
																<p class="text-muted text-small mb-1">Cosent Sub Category :</p>
																<p>
																	การให้ความยินยอมเปิดผยข้อมูลแก่บริษัทในกลุ่มธุรกิฉทางการเงินของธนาคาร
																</p>
																<p>
																	การให้ความยินยอมปิดผยข้อมูลเก่บริษัทนอกกลุ่มธุรกิจทางการเงิน
																</p>
															</div>
														</div>
														<div class="row mb-0">
															<div class="col">
																<p class="text-muted text-small mb-1">Purpose of Consent :</p>
																<p class="mb-0">
																	ข้าพเจ้ายอมรับในเงื่อนไขดังนี้
																</p>
																<ol class="list-inline-item pl-3">
																	<li>ข้าพเจ้ามีความประสงค์ให้ เอไอเอ หักงินจากบัญชีงินฝากธนาคารข้างต้นเพื่อชำระเบี้ยประกันภัยของวดปัจุบัน รวมทั้งเบี้ยประกันภัยงวดต่อไปของกรมธรรม์ตามที่ เอไอเอ     
ได้พิจารณาออกให้ตามใบคำขอเอาประกันภัยเลขที่ดังกล่าวข้างต้น ทั้งนี้ จนกว่ากรมธรรดังกล่าวจะถูกบอกเลิกหรือสิ้นสุดลง</li>
<li>กรณีที่บัญชีงินฝากธนาคารที่ระบุ หรือเบี้ยประกันภัยได้เปลี่ยนแปลงไม่ว่าโดยสาเหตุใดก็ตาม หนังสือฉบับนี้ ยังคงมีผลบังคับใช้กับบัญชีธนาคารซึ่งเป็นบัญชีใหม่หรือเบี้ยประกันภัยที่ได้เปลี่ยนแปลงนั้นๆ  
ทุกประการ</li>
<li>ข้าพเจ้ายอมรับว่า หากธนาคารไม่สามารถหักบัญชีตามที่ระบุไว้ไม่ว่าโดยสาเหตุใดก็ตามให้ถือเสมือนหนึ่งว่ายังไม่มีการชำระเบี้ยประกันภัยของกรมรรม์ประกันภัยข้างต้น</li>
<li>กรมธรรที่ระบุข้างต้น จะต้องระบุชื่อเจ้าของบัญชี หรือบุคคลในครอบครัวของเจ้าของบัญชี เป็นผู้เอาประกันภัย เจ้าของกรมธรรม์และเหรือผู้ชำระเบี้ยประกันภัย</li>
<li>การหักบัญชีธนาคารดังกล่าวข้างต้นให้มีผลบังคับนับแต่วันที่เอไอเอทำการอนุมัติ และให้คงมีผลบังคับต่อไปจนกว่าข้าพเจ้าจะได้เพิกถอนโดยทำเป็นลายลักษณ์
อักษรส่งให้ธนาคารหรือเอไอเอทราบล่วงหน้าอย่างน้อย 30 วัน</li>
																</ol>
															</div>

														</div>

														<div class="row mb-0">
															<div class="col-sm-4">
																<p class="text-muted text-small mb-1">Consent Start Date:</p>
																<p>
																	19-05-2020
																</p>
															</div>
															<div class="col-sm-4">
																<p class="text-muted text-small mb-1">Consent Expire Date:</p>
																<p>
																	19-05-2023
																</p>
															</div>
															<div class="col-sm-4">
																<p class="text-muted text-small mb-1">Consent Version:</p>
																<p>
																	1.0 <a  href="javascript:;"  data-toggle="modal" data-backdrop="static" data-target="#History" class="text-primary">View version history </a>
																</p>
															</div>
														</div>
														<div class="row mb-0">
															<div class="col-sm-4">
																<p class="text-muted text-small mb-1">Other Document File  :</p>
																<p>
																	<a href="#" class="text-primary">สำเนาบัตรประชาชน.pdf</a><i class="glyph-icon iconsminds-close"></i>
																</p>
															</div>
															<div class="col-sm-4">
																<p class="text-muted text-small mb-1">Consent file :</p>
																<p>
																	<a href="#" class="text-primary">tidlor_2020_loan_agreement.pdf</a>
																</p>
																<p><button class="btn btn-outline-primary btn-sm" type="button">Resend  E-mail </button></p>
															</div>
															<div class="col-sm-4">
																&nbsp;
															</div>
														</div>
														
													</div>

													<div class="bg-white">

														<div class="user-info-inner">
															<div class="row mb-0">
																<div class="col-sm-4">
																	<p class="text-muted text-small mb-1">Name Lastname (thai) :</p>
																	<p>
																		นาย เอทต้า เกย์กลอลี่
																	</p>
																</div>
																<div class="col-sm-4">
																	<p class="text-muted text-small mb-1">Name Lastname (English) :</p>
																	<p>
																		Mr. Etta Gregory
																	</p>
																</div>
																<div class="col-sm-4">
																	<p class="text-muted text-small mb-1">Gender :</p>
																	<p>
																		Male
																	</p>
																</div>
															</div>

															<div class="row mb-0">

																<div class="col-sm-4">
																	<p class="text-muted text-small mb-1">Birth Date :</p>
																	<p>
																		15-09-1984
																	</p>
																</div>

																<div class="col-sm-4">
																	<p class="text-muted text-small mb-1">ID Card :</p>
																	<p>
																		98056999087334
																	</p>
																</div>
																<div class="col-sm-4">
																	<p class="text-muted text-small mb-1">Phone Number :</p>
																	<p>
																		0984456678
																	</p>
																</div>
															</div>

															<div class="row mb-0">
																<div class="col">
																	<p class="text-muted text-small mb-1">Email :</p>
																	<p>
																		sarun.s@gmail.com
																	</p>
																</div>

															</div>

															<div class="separator mb-3"></div>


															<div class="row mb-0">
																<div class="col-sm-4">
																	<p class="text-muted text-small mb-1">Address Line1 :</p>
																	<p>
																		437/109 ลุมพินีคอนโด  ถนนรัตนาธิเบศร์
																	</p>
																</div>
																<div class="col-sm-4">
																	<p class="text-muted text-small mb-1">Address Line2 :</p>
																	<p>
																		-
																	</p>
																</div>
																<div class="col-sm-4">
																	<p class="text-muted text-small mb-1">Sub District :</p>
																	<p>
																		บางกระสอ
																	</p>
																</div>
															</div>

															<div class="row mb-0">

																<div class="col-sm-4">
																	<p class="text-muted text-small mb-1">District :</p>
																	<p>
																		เมืองนนทบุรี
																	</p>
																</div>

																<div class="col-sm-4">
																	<p class="text-muted text-small mb-1">Province :</p>
																	<p>
																		นนทบุรี
																	</p>
																</div>
																<div class="col-sm-4">
																	<p class="text-muted text-small mb-1">Zipcode :</p>
																	<p>
																		101110
																	</p>
																</div>
															</div>

														</div>
													</div>

											   </div>

											</div>
										</div>


									</div>
								</div>
						</div>
						
						
					</div>
					<!-- /tabs-zone -->
					
                </div>
				
				<!-- Modal -->
				<!-- Overview modal -->
                            <div class="modal fade" id="overView" tabindex="-1" role="dialog"
                                aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-xl" role="document">
                                    <div class="modal-content rounded-1">
                                        <div class="modal-header pb-0 border-bottom-0">
                                            <h5 class="modal-title"><b>Over view : </b><span class="text-muted text-small">Phone Number (2)</span></h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>
                                        <div class="modal-body pt-1">
											
											<div class="card-body p-0">
                                                       <div class="bg-light p-2 mb-4 d-flex justify-content-between flex-nowrap align-items-center">
													   		<ul class="list-inline d-flex mb-0 justify-content-start">
																<li class="mr-4"><span class="text-muted">Information : </span><span>1</span></li>
																<li class="mr-4"><span class="text-muted">Product ID : </span><span>00345</span></li>
																<li class="mr-4"><span class="text-muted">Last Update : </span><span>19-05-2020  10:30</span></li>
																<li class="mr-4"><span class="text-muted">Status : </span><span class="text-primary">Active</span></li>
															</ul>
															<div class="ctrl-btn">
																<button class="btn btn-primary btn-sm" type="button">Edit </button>
																<button class="btn btn-outline-primary btn-sm" type="button">Delete </button>
															</div>
													   </div>
													   
													   <div class="detail-card">
													   		<div class="bg-white">
																<div class="row mb-0">
																	<div class="col-sm-4">
																		<p class="text-muted text-small mb-1">Phone Number :</p>
																		<p>
																			0898890098
																		</p>
																	</div>
																	<div class="col-sm-4">
																		<p class="text-muted text-small mb-1">Product Name :</p>
																		<p>
																			Loan
																		</p>
																	</div>
																	<div class="col-sm-4">
																		&nbsp;
																	</div>
																</div>
																<div class="row mb-0">
																	<div class="col-sm-4">
																		<p class="text-muted text-small mb-1">System Name :</p>
																		<p>
																			MBK Loan system management
																		</p>
																	</div>
																	<div class="col-sm-4">
																		<p class="text-muted text-small mb-1">Customer Type :</p>
																		<p>
																			Customer
																		</p>
																	</div>
																	<div class="col-sm-4">
																		<p class="text-muted text-small mb-1">Channel Input :</p>
																		<p>
																			Call API
																		</p>
																	</div>
																</div>
															</div>
															
															<div class="bg-white mb-4">
																<div class="row mb-0">
																	<div class="col-sm-4">
																		<p class="text-muted text-small mb-1">Cosent ID :</p>
																		<p>
																			1009654
																		</p>
																	</div>
																	<div class="col-sm-4">
																		<p class="text-muted text-small mb-1">Consent Category  :</p>
																		<p>
																			หนังสือให้ความยินยอมเปิดเผยข้อมูล
																		</p>
																	</div>
																	<div class="col-sm-4">
																		<p class="text-muted text-small mb-1">Cosent Sub Category :</p>
																		<p>
																			การให้ยินยอมให้เปิดเผยข้อมูลเฉพาะบางกรณีในกลุ่มธุรกิจการเงิน
																		</p>
																	</div>
																</div>
																<div class="row mb-0">
																	<div class="col">
																		<p class="text-muted text-small mb-1">Purpose of Consent :</p>
																		<p>
																			ตกลงยินยอมให้ บมจ.xxxx  เปิดผข้อมูลส่วนบุกกของช้พจ เช่น ชื่อ ช่องทางในการติดต่อเพศอายุการศึกษา เป็นต้น แต่ทั้งนี้ ไม่รวมถึงข้อมูลที่เกี่ยวกับบัญชีของข้พเจ้าเช่น เลขที่บัญชี ยอดคงเหลือในบัญชี รายการเคลื่อนไหวในบัญชีเป็นต้น ให้แก่กลุ่มธุรกิจทางการเงินของธนาคารและให้กลุ่มธุรกิจทางการเงินของธนาคารสามารถใช้ข้มูลดังกล่ว พื่อวัตถุประสงค์ทางการตลาด เช่น เพื่อวัตถุประสงค์ในการพิจารณานำเสนอผลิตภัณฑ์ หรือเพื่อส่งเสริมการขายผลิตภัณฑ์บริกร และข้อสนอพิเศษอื่นๆ ของบริษัทในกลุ่มธุรกิจการเงินของธนาคาร ให้แก่ข้าพเจ้า
																		</p>
																	</div>
																	
																</div>
																
																<div class="row mb-0">
																	<div class="col-sm-4">
																		<p class="text-muted text-small mb-1">Consent Start Date:</p>
																		<p>
																			19-05-2020
																		</p>
																	</div>
																	<div class="col-sm-4">
																		<p class="text-muted text-small mb-1">Consent Expire Date:</p>
																		<p>
																			19-05-2023
																		</p>
																	</div>
																	<div class="col-sm-4">
																		<p class="text-muted text-small mb-1">Consent Version:</p>
																		<p>
																			5.0 <a href="javascript:;" data-toggle="modal" data-backdrop="static" data-target="#History" class="text-primary">View version history </a>
																		</p>
																	</div>
																</div>
																<div class="row mb-0">
																	<div class="col-sm-4">
																		<p class="text-muted text-small mb-1">Other Document File  :</p>
																		<p class="mb-0">
																			<a href="#" class="text-primary">สำเนาบัตรประชาชน.pdf</a><i class="glyph-icon iconsminds-close"></i>
																		</p>
																		<p class="mb-0">
																			<a href="#" class="text-primary">สำเนาทะเบียนบ้าน.pdf</a><i class="glyph-icon iconsminds-close"></i>
																		</p>
																		<p class="mb-0">
																			<a href="#" class="text-primary">สำเนาบุ๊คแบงค์ KTB.pdf</a><i class="glyph-icon iconsminds-close"></i>
																		</p>
																	</div>
																	<div class="col-sm-4">
																		<p class="text-muted text-small mb-1">Consent file :</p>
																		<p>
																			<a href="#" class="text-primary">MBK_2020_loan_agreement.pdf</a>
																		</p>
																		<p><button class="btn btn-outline-primary btn-sm" type="button">Resend  E-mail </button></p>
																	</div>
																	<div class="col-sm-4">
																		&nbsp;
																	</div>
																</div>
																
															</div>
															
															
															
													   </div>
													   
													   <div class="space mb-5"></div>
													   
													    <div class="bg-light p-2 mb-4 d-flex justify-content-between flex-nowrap align-items-center">
													   		<ul class="list-inline d-flex mb-0 justify-content-start">
																<li class="mr-4"><span class="text-muted">Information : </span><span>2</span></li>
																<li class="mr-4"><span class="text-muted">Product ID : </span><span>00345</span></li>
																<li class="mr-4"><span class="text-muted">Last Update : </span><span>19-05-2020  10:30</span></li>
																<li class="mr-4"><span class="text-muted">Status : </span><span class="text-primary">Active</span></li>
															</ul>
															<div class="ctrl-btn">
																<button class="btn btn-primary btn-sm" type="button">Edit </button>
																<button class="btn btn-outline-primary btn-sm" type="button">Delete </button>
															</div>
													   </div>
													   
													   <div class="detail-card">
													   		<div class="bg-white">
																<div class="row mb-0">
																	<div class="col-sm-4">
																		<p class="text-muted text-small mb-1">Phone Number :</p>
																		<p>
																			0898890098
																		</p>
																	</div>
																	<div class="col-sm-4">
																		<p class="text-muted text-small mb-1">Product Name :</p>
																		<p>
																			Life Insurance
																		</p>
																	</div>
																	<div class="col-sm-4">
																		&nbsp;
																	</div>
																</div>
																<div class="row mb-0">
																	<div class="col-sm-4">
																		<p class="text-muted text-small mb-1">System Name :</p>
																		<p>
																			www.ngerntidlor.com
																		</p>
																	</div>
																	<div class="col-sm-4">
																		<p class="text-muted text-small mb-1">Customer Type :</p>
																		<p>
																			Customer
																		</p>
																	</div>
																	<div class="col-sm-4">
																		<p class="text-muted text-small mb-1">Channel Input :</p>
																		<p>
																			Call API
																		</p>
																	</div>
																</div>
															</div>
															
															<div class="bg-white mb-4">
																<div class="row mb-0">
																	<div class="col-sm-4">
																		<p class="text-muted text-small mb-1">Cosent ID :</p>
																		<p>
																			1009654
																		</p>
																	</div>
																	<div class="col-sm-4">
																		<p class="text-muted text-small mb-1">Consent Category  :</p>
																		<p>
																			หนังสือให้ความยินยอมเปิดเผยข้อมูล
																		</p>
																	</div>
																	<div class="col-sm-4">
																		<p class="text-muted text-small mb-1">Cosent Sub Category :</p>
																		<p>
																			การให้ยินยอมให้เปิดเผยข้อมูลเฉพาะบางกรณีในกลุ่มธุรกิจการเงิน
																		</p>
																	</div>
																</div>
																<div class="row mb-0">
																	<div class="col">
																		<p class="text-muted text-small mb-1">Purpose of Consent :</p>
																		<p>
																			ตกลงยินยอมให้ บมจ.xxxx  เปิดผข้อมูลส่วนบุกกของช้พจ เช่น ชื่อ ช่องทางในการติดต่อเพศอายุการศึกษา เป็นต้น แต่ทั้งนี้ ไม่รวมถึงข้อมูลที่เกี่ยวกับบัญชีของข้พเจ้าเช่น เลขที่บัญชี ยอดคงเหลือในบัญชี รายการเคลื่อนไหวในบัญชีเป็นต้น ให้แก่กลุ่มธุรกิจทางการเงินของธนาคารและให้กลุ่มธุรกิจทางการเงินของธนาคารสามารถใช้ข้มูลดังกล่ว พื่อวัตถุประสงค์ทางการตลาด เช่น เพื่อวัตถุประสงค์ในการพิจารณานำเสนอผลิตภัณฑ์ หรือเพื่อส่งเสริมการขายผลิตภัณฑ์บริกร และข้อสนอพิเศษอื่นๆ ของบริษัทในกลุ่มธุรกิจการเงินของธนาคาร ให้แก่ข้าพเจ้า
																		</p>
																	</div>
																	
																</div>
																
																<div class="row mb-0">
																	<div class="col-sm-4">
																		<p class="text-muted text-small mb-1">Consent Start Date:</p>
																		<p>
																			19-05-2020
																		</p>
																	</div>
																	<div class="col-sm-4">
																		<p class="text-muted text-small mb-1">Consent Expire Date:</p>
																		<p>
																			19-05-2023
																		</p>
																	</div>
																	<div class="col-sm-4">
																		<p class="text-muted text-small mb-1">Consent Version:</p>
																		<p>
																			5.0 <a href="javascript:;" data-toggle="modal" data-backdrop="static" data-target="#History" class="text-primary">View version history </a>
																		</p>
																	</div>
																</div>
																<div class="row mb-0">
																	<div class="col-sm-4">
																		<p class="text-muted text-small mb-1">Other Document File  :</p>
																		<p class="mb-0">
																			<a href="#" class="text-primary">สำเนาบัตรประชาชน.pdf</a><i class="glyph-icon iconsminds-close"></i>
																		</p>
																		<p class="mb-0">
																			<a href="#" class="text-primary">สำเนาทะเบียนบ้าน.pdf</a><i class="glyph-icon iconsminds-close"></i>
																		</p>
																		<p class="mb-0">
																			<a href="#" class="text-primary">สำเนาบุ๊คแบงค์ KTB.pdf</a><i class="glyph-icon iconsminds-close"></i>
																		</p>
																	</div>
																	<div class="col-sm-4">
																		<p class="text-muted text-small mb-1">Consent file :</p>
																		<p>
																			<a href="#" class="text-primary">MBK_2020_loan_agreement.pdf</a>
																		</p>
																		<p><button class="btn btn-outline-primary btn-sm" type="button">Resend  E-mail </button></p>
																	</div>
																	<div class="col-sm-4">
																		&nbsp;
																	</div>
																</div>
																
															</div>
															
															
															
													   </div>
													   
													   
                                                    </div>
										</div>
									</div>
								</div>
							</div>
				<!-- /Overview modal -->
				<!-- History Modal -->
				<div class="modal fade " id="History" tabindex="-1" role="dialog"
                                aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-xl" role="document">
                                    <div class="modal-content rounded-1">
                                        <div class="modal-header">
                                            <h5 class="modal-title">Version History</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
										<table class="table table-hover responsive nowrap">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Consent Version</th>
                                        <th>Cosent Category</th>
                                        <th>Cosent Sub Category</th>
										<th>Purpose of Consent</th>
										<th>Start date</th>
										<th>Expire date</th>
										
                                    </tr>
                                </thead>
                                <tbody>
									<?php for($i=1;$i<=3;$i++){?> 
                                    <tr>
                                        <td>
                                            <p><?php echo $i; ?></p>
                                        </td>
                                        <td class="text-center">
                                            <p>2.0</p>
                                        </td>
										<td class="text-center">
                                            <p>Marketing</p>
                                        </td>
										<td class="text-center">
                                            <p>Promotion &amp; Marketing</p>
                                        </td>
										<td>
                                            <p>We create data-driven digital strategies that are 
rooted in marketing fundamentals. We deliver truly 
integrated campaigns that achieve business results</p>
                                        </td>
										<td class="text-center">
                                            <p>15-05-2020</p>
                                        </td>
										<td class="text-center">
                                            <p>15-05-2030</p>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
										</div>
									</div>
								</div>
				</div>
				<!-- /Histor Modal -->
            </div>
        </div>

    </main>

    

    <?php include("incs/js.html") ?>
    <script src="js/vendor/bootstrap-tagsinput.min.js"></script>
	<script src="js/vendor/datatables.min.js"></script>
    <script src="js/dore.script.js"></script>
    <script src="js/scripts.js"></script>
	<script>
/*	$(document).ready(function() {
    $('#tablelist').DataTable( {
        "paging":   false,
        "ordering": false,
        "info":     false
    } );
} );*/
	</script>
</body>

</html>
<!DOCTYPE html>
<html lang="en">

<!-- Top Head -->
<?php include("incs/header-top.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-sub-hidden show-spinner">
    <?php include("incs/header.html") ?>
    <?php include("incs/sidebar-left.html") ?>

    <main>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="mb-2">
                        <h1>Dashboards</h1>
						<?php /*?><nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
							<ol class="breadcrumb pt-0">
								<!--<li class="breadcrumb-item">
									<a href="#">Home</a>
								</li>
								<li class="breadcrumb-item">
									<a href="#">Library</a>
								</li>-->
								<li class="breadcrumb-item active text-gray" aria-current="page">Consent System Report </li>
							</ol>
						</nav><?php */?>
                        <!--<div class="top-right-button-container">

                            <div class="btn-group">
                                <div class="btn btn-primary btn-lg pl-4 pr-0 check-button">
                                    <label class="custom-control custom-checkbox mb-0 d-inline-block">
                                        <input type="checkbox" class="custom-control-input" id="checkAll">
                                        <span class="custom-control-label">&nbsp;</span>
                                    </label>
                                </div>
                                <button type="button"
                                    class="btn btn-lg btn-primary dropdown-toggle dropdown-toggle-split"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a class="dropdown-item" href="#">Action</a>
                                    <a class="dropdown-item" href="#">Another action</a>
                                </div>
                            </div>
                        </div>-->
                    </div>

                    <div class="mb-2 d-flex justify-content-between">
						<div class="col-l">
							<a class="btn pt-0 pl-0 d-inline-block d-md-none" data-toggle="collapse" href="#displayOptions"
								role="button" aria-expanded="true" aria-controls="displayOptions">
								Display Options
								<i class="simple-icon-arrow-down align-middle"></i>
							</a>
							<div class="collapse d-md-block" id="displayOptions">
								<div class="d-flex align-items-end">
									
									<div class="float-md-left mr-2 mb-1 dropdown-as-select">
										
										<button class="btn btn-outline-dark btn-xs dropdown-toggle" type="button"
											data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											Select display date : Today
										</button>
										<div class="dropdown-menu">
											<a class="dropdown-item active" href="#">Select display date : Today</a>
											<a class="dropdown-item" href="#">Another action</a>
										</div>
									</div>
									
								

								</div>
							</div>
						</div>
						<!--<div class="col-r pb-2 top-right-button-container d-flex align-items-end">
                            <button type="button" class="btn bg-green btn-lg top-right-button btn-export mr-1"><i class="glyph-icon iconsminds-receipt-4"></i> Export  Excel</button>

						</div>-->
					</div>

                    <div class="separator mb-5"></div>
					
					<div class="row mb-4">
					<div class="col-12">
					<div class="row">
						<div class="col-md-8 icon-cards-row">
							<div class="glide dashboard-numbers h-100">

									<ul class="list-inline row d-flex h-100">
										<li class="col h-100">
											<a href="#" class="card h-100 bg-primary">
												<div class="card-body text-white d-flex flex-column justify-content-center align-items-center">
													<i class="icon mb-3"><img src="di/ic-current.png" height="60"></i>
													<p class="card-text mb-0">Current Consent</p>
													<big class="lead text-center">500</big>
												</div>
											</a>
										</li>
										<li class="col h-100">
											<a href="#" class="card h-100 bg-success">
												<div class="card-body text-white d-flex flex-column justify-content-center align-items-center">
													<i class="icon mb-3"><img src="di/ic-new-consent.png" height="60"></i>
													<p class="card-text mb-0">New Consent</p>
													<p class="lead text-center">+300</p>
												</div>
											</a>
										</li>
										<li class="col h-100">
											<a href="#" class="card h-100 bg-gray">
												<div class="card-body text-white d-flex flex-column justify-content-center align-items-center">
													<i class="icon mb-3"><img src="di/ic-expire.png" height="60"></i>
													<p class="card-text mb-0">Expire Consent</p>
													<p class="lead text-center">-200</p>
												</div>
											</a>
										</li>
										<li class="col h-100">
											<a href="#" class="card h-100 bg-danger">
												<div class="card-body text-white d-flex flex-column justify-content-center align-items-center">
													<i class="icon mb-3"><img src="di/ic-cancel.png" height="60"></i>
													<p class="card-text mb-0">Cancel Consent</p>
													<p class="lead text-center">-50</p>
												</div>
											</a>
										</li>
									</ul>
			
							</div>
						</div>
						<div class="col-md-4">
							<div class="card h-100">
							<ul class="list-inline m-0 list-total">
								<li class=" d-flex justify-content-between align-items-center flex-nowrap p-4">
									<p class="mr-2 mb-0">Total System</p>
									<big class="text-success">10</big>
								</li>
								<li class="bg-light d-flex justify-content-between align-items-center flex-nowrap p-4">
									<p class="mr-2 mb-0">Total Channel Input</p>
									<big class="text-success">5</big>
								</li>
								<li class=" d-flex justify-content-between align-items-center flex-nowrap p-4">
									<p class="mr-2 mb-0">Total Channel Input</p>
									<big class="text-success">250</big>
								</li>
							</ul>
							</div>
						</div>
					</div>
					</div>
					</div>
					
					<div class="row">
						<div class="col-12 mb-4">
							<div class="card">
								<div class="card-body">
						
									
									<div class="float-left float-none-xs">
										<div class="d-inline-block">
											<h5 class="d-inline">Consent Graph</h5>
											<!--<span class="text-muted text-small d-block">Unique Visitors</span>-->
										</div>
									</div>
									<div class="btn-group float-right float-none-xs mt-2">
										<button class="btn btn-outline-primary btn-xs dropdown-toggle" type="button"
											data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											This Week
										</button>
										<div class="dropdown-menu">
											<a class="dropdown-item" href="#">Last Week</a>
											<a class="dropdown-item" href="#">This Month</a>
										</div>
									</div>
									
								</div>
								<div class="card-body">
									<div class="dashboard-line-chart chart">
										<canvas id="salesChart"></canvas>
									</div>
								</div>
							</div>
						</div>
					</div>
					
                </div>
            </div>
        </div>

    </main>

    

    <?php include("incs/js.html") ?>
	
	 <script src="js/vendor/Chart.bundle.min.js"></script>
    <script src="js/vendor/chartjs-plugin-datalabels.js"></script>
	 
    <script src="js/dore.script.js"></script>
    <script src="js/scripts.js"></script>
	<script>
/*	$(document).ready(function() {
    $('#tablelist').DataTable( {
        "paging":   false,
        "ordering": false,
        "info":     false
    } );
} );*/
	</script>
		<script>
$(document).ready(function() {
	$('.main-menu .list-unstyled>li').removeClass('active');
	$('.main-menu .list-unstyled>li:nth-child(1)').addClass('active');
});
</script>
</body>

</html>
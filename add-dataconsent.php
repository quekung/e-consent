<!DOCTYPE html>
<html lang="en">

<!-- Top Head -->
<?php include("incs/header-top.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-sub-hidden show-spinner">
    <?php include("incs/header.html") ?>
    <?php include("incs/sidebar-left.html") ?>

    <main>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="mb-2">
                        <h1>Add Data Consent</h1>
						<nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
							<ol class="breadcrumb pt-0">
								<!--<li class="breadcrumb-item">
									<a href="#">Home</a>
								</li>-->
								<li class="breadcrumb-item">
									<a href="#">Consent Person List</a>  | 
								</li>
								<li class="breadcrumb-item active text-gray" aria-current="page">Add Data Consent</li>
							</ol>
						</nav>
                        <!--<div class="top-right-button-container">

                            <div class="btn-group">
                                <div class="btn btn-primary btn-lg pl-4 pr-0 check-button">
                                    <label class="custom-control custom-checkbox mb-0 d-inline-block">
                                        <input type="checkbox" class="custom-control-input" id="checkAll">
                                        <span class="custom-control-label">&nbsp;</span>
                                    </label>
                                </div>
                                <button type="button"
                                    class="btn btn-lg btn-primary dropdown-toggle dropdown-toggle-split"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a class="dropdown-item" href="#">Action</a>
                                    <a class="dropdown-item" href="#">Another action</a>
                                </div>
                            </div>
                        </div>-->
                    </div>



                    <div class="separator mb-5"></div>
					<!-- Topzone -->
					<div class="top-zone">
						<h2 class="sub-head font-weight-bold text-medium mb-3">Consent Person</h2>
						<div class="row">
								 <div class="col-12 mb-4">

                                    <div class="card question d-flex h-100 edit-quesiton">
                                                <div class="card-title pt-4 pl-4 pr-4 mb-0 d-flex justify-content-between min-width-zero">

                                                    <div class="align-self-center d-flex flex-column flex-md-row justify-content-between min-width-zero align-items-md-center">
                                                       <h5 class=" font-weight-bold">Master Information</h5>
                                                    </div>
                                                    <div class="custom-control custom-checkbox pl-1 align-self-center">
															<a class="col-12 btn btn-primary btn-xs" href="javascript:;" data-toggle="modal" data-backdrop="static" data-target="#addConsent"> + Add Consent</a>
                                                    </div>
                                                </div>
                                                <div class="question-collapse collapse show">
                                                    <div class="card-body row">
                                     					<div class="pr-3 pl-2 d-flex justify-content-center justify-content-xl-start">
															<div class="user-avatar">
															<figure>
																<label for="upload-image">
																<img src="di/bg-add-photo.png" class="img-thumbnail rounded-2">
																</label>
																<input type="file" id="upload-image" name="upload-image" class="text-hide position-absolute invisible">
															</figure>
															<!--<div class="mt-4 text-center">
																<a href="javascript:;" title="Edit Information" class="btn btn-outline-primary btn-block">Edit Information</a>
															</div>-->
															</div>
														</div>
														<div class="user-info col">
															<div class="row mb-0">
																<div class="col-xs-1">
																	<p class="text-muted text-small mb-1">คำนำหน้า :</p>
																	<p>
																		<select class="form-control select2-normal" data-width="100%">
																			<option value="0" selected>นาย</option>
																			<option value="1">นาง</option>
																			<option value="2">นางสาว</option>
																		</select>
																	</p>
																</div>
																<div class="col-xs-12 col-sm">
																	<p class="text-muted text-small mb-1">Name (thai) :</p>
																	<p>
																		<input type="text" class="form-control" placeholder="ชื่อ">
																	</p>
																</div>
																<div class="col-xs-12 col-sm">
																	<p class="text-muted text-small mb-1">Lastname (thai) :</p>
																	<p>
																		<input type="text" class="form-control" placeholder="สกุล">
																	</p>
																</div>
															</div>
															
															<div class="row mb-0">
																<div class="col-xs-1">
																	<p class="text-muted text-small mb-1">&nbsp;</p>
																	<p>
																		<select class="form-control select2-normal" data-width="100%">
																			<option value="0" selected>Mr.</option>
																			<option value="1">Mrs.</option>
																			<option value="2">Miss</option>
																		</select>
																	</p>
																</div>
																<div class="col-xs-12 col-sm">
																	<p class="text-muted text-small mb-1">Name (English) :</p>
																	<p>
																		<input type="text" class="form-control" placeholder="ชื่อ">
																	</p>
																</div>
																<div class="col-xs-12 col-sm">
																	<p class="text-muted text-small mb-1">Lastname (English) :</p>
																	<p>
																		<input type="text" class="form-control" placeholder="สกุล">
																	</p>
																</div>
															</div>
															
															<div class="row mb-0">
																<div class="col-xs-1">
																	<p class="text-muted text-small mb-1">Gender</p>
																	<p>
																		<select class="form-control select2-normal" data-width="100%">
																			<option value="0" selected>เพศ</option>
																			<option value="1">Male</option>
																			<option value="2">Female</option>
																		</select>
																	</p>
																</div>
																<div class="col-xs-12 col-sm">
																	<p class="text-muted text-small mb-1">Birth Date :</p>
						
																		<div class="input-group date">
																			<input type="text" class="form-control" placeholder="dd/mm/yyyy">
																			<span class="input-group-text input-group-append input-group-addon">
																				<i class="simple-icon-calendar"></i>
																			</span>
																		</div>
																		<!--<input type="text" class="form-control" placeholder="dd/mm/yyyy">-->
																	
																</div>
																<div class="col-xs-12 col-sm">
																	<p class="text-muted text-small mb-1">ID Card :</p>
																	<p>
																		<input type="text" class="form-control" placeholder="ID Card">
																	</p>
																</div>
																<div class="col-xs-12 col-sm">
																	<p class="text-muted text-small mb-1">Phone Number :</p>
																	<p>
																		<input type="text" class="form-control" placeholder="xxx-xxx-xxxx">
																	</p>
																</div>
															</div>
				
															
															<div class="row mb-0">
																<div class="col-sm-4">
																	<p class="text-muted text-small mb-1">Email :</p>
																	<p>
																		<input type="text" class="form-control" placeholder="E-mail">
																	</p>
																</div>
																<div class="col-sm-6">
																	&nbsp;
																</div>
															</div>
															
															<div class="separator mb-3"></div>
															
															<div class="row mb-0">
																<div class="col-sm-6">
																	<p class="text-muted text-small mb-1">Address Line1 :</p>
																	<p>
																		<input type="text" class="form-control" placeholder="">
																	</p>
																</div>
																<div class="col-sm-6">
																	<p class="text-muted text-small mb-1">Address Line2 :</p>
																	<p>
																		<input type="text" class="form-control" placeholder="">
																	</p>
																</div>
															</div>
															
															<div class="row mb-0">
																<div class="col-sm-3">
																	<p class="text-muted text-small mb-1">Sub District :</p>
																	<p>
																		<select class="form-control select2-normal" data-width="100%">
																			<option value="0" selected>select</option>
																			<option value="1">option1</option>
																			<option value="2">option2</option>
																		</select>
																	</p>
																</div>
																<div class="col-sm-3">
																	<p class="text-muted text-small mb-1">District :</p>
																	<p>
																		<select class="form-control select2-normal" data-width="100%">
																			<option value="0" selected>select</option>
																			<option value="1">option1</option>
																			<option value="2">option2</option>
																		</select>
																	</p>
																</div>
															
																<div class="col-sm-3">
																	<p class="text-muted text-small mb-1">Province :</p>
																	<p>
																		<select class="form-control select2-normal" data-width="100%">
																			<option value="0" selected>select</option>
																			<option value="1">option1</option>
																			<option value="2">option2</option>
																		</select>
																	</p>
																</div>
																<div class="col-sm-3">
																	<p class="text-muted text-small mb-1">Zipcode :</p>
																	<p>
																		<input type="text" class="form-control" placeholder="">
																	</p>
																</div>
															</div>
															
															<div class="ctrl-btn d-flex justify-content-end">
																<button class="btn btn-primary mr-2 btn-sm" type="button">Submit</button>
																<button class="btn btn-outline-primary btn-sm" type="button">Cancel</button>
															</div>
														
														</div>
														

                                                        

                                                    </div>
                                                </div>
                                            </div>
                                </div>



                               
                            </div>
					</div>
					
					<!-- Tabs-zone -->
					<div class="type-add">
						   <div class="card product1 d-flex mb-4 edit-product1">
												
												  
												  
											
                                                <div class="head-bar d-flex flex-grow-1 min-width-zero">
                                                    <div class="card-body align-self-center d-flex flex-column flex-md-row justify-content-between min-width-zero align-items-md-center">
                                                        <div class="list-item-heading mb-0 truncate w-80 mb-1 mt-1">
                                                           <div class="type d-flex">
																<!--<div class="custom-control custom-radio mr-5">
																	<input type="radio" id="t1" name="type_add" class="custom-control-input" required="">
																	<label class="custom-control-label" for="t1">Products
																		radio</label>
																</div>
																<div class="custom-control custom-radio">
																	<input type="radio" id="t2" name="type_add" class="custom-control-input" required="">
																	<label class="custom-control-label" for="t2"> Contract
																		radio</label>
																</div>-->
																<span class="mr-5"><label for="t1" class="t-gray2"><input type="radio" name="type_add" id="t1" value="0" checked> Products</label></span>
																<span><label for="t2" class="t-gray2"><input type="radio" name="type_add" id="t2" value="1"> Contract</label></span>
															  </div>
                                                        </div>
                                                    </div>
                                                    <!--<div class="custom-control custom-checkbox pl-1 head-bar align-self-center pr-4">
                                                        
                                                        <button class="btn btn-outline-theme-3 icon-button rotate-icon-click rotate" type="button" data-toggle="collapse" data-target="#p1" aria-expanded="false" aria-controls="p1">
                                                            <i class="simple-icon-arrow-down with-rotate-icon"></i>
                                                        </button>
                                                    </div>-->
                                                </div>
												
												<div class="for-t1">
												  <div class="product1-collapse collapse show" id="p1">
                                                    <div class="card-body pt-0 pl-4 pr-4">
														<div class="d-flex align-items-center mb-4">
															<b>Product  Name : </b> 
															<div class="col-sm-5 col-lg-3">
																<select class="form-control select2-normal" data-width="100%">
																	<option value="0" selected>Loan</option>
																	<option value="1">option1</option>
																	<option value="2">option2</option>
																</select>
															</div>
														</div>
													   
													   <div class="detail-card">
													   		<div class="bg-white">
																<div class="row mb-0">
																	<div class="col-sm-4">
																		<p class="text-muted text-small mb-1">System Name :</p>
																		<p>
																			<select class="form-control select2-normal" data-width="100%">
																				<option value="0" selected>MBK Loan system management</option>
																				<option value="1">option1</option>
																				<option value="2">option2</option>
																			</select>
																		</p>
																	</div>
																	<div class="col-sm-4">
																		<p class="text-muted text-small mb-1">Customer Type :</p>
																		<p>
																			<select class="form-control select2-normal" data-width="100%">
																				<option value="0" selected>Customer</option>
																				<option value="1">option1</option>
																				<option value="2">option2</option>
																			</select>
																		</p>
																	</div>
																	<div class="col-sm-4">
																		<p class="text-muted text-small mb-1">Channel Input :</p>
																		<p>
																			<select class="form-control select2-normal" data-width="100%">
																				<option value="0" selected>Call API</option>
																				<option value="1">option1</option>
																				<option value="2">option2</option>
																			</select>
																		</p>
																	</div>
																</div>
															</div>
															
															<div class="bg-white mb-4">
																<div class="row mb-0">
																	<div class="col-sm-4">
																		<p class="text-muted text-small mb-1">Cosent ID :</p>
																		<p>
																			<select class="form-control select2-normal" data-width="100%">
																				<option value="0" selected>1009654</option>
																				<option value="1">option1</option>
																				<option value="2">option2</option>
																			</select>
																		</p>
																	</div>
																	<div class="col-sm-4">
																		<p class="text-muted text-small mb-1">Consent Category  :</p>
																		<p>
																			<select class="form-control select2-normal" data-width="100%">
																				<option value="0" selected>หนังสือให้ความยินยอมเปิดเผยข้อมูล</option>
																				<option value="1">option1</option>
																				<option value="2">option2</option>
																			</select>
																		</p>
																	</div>
																	<div class="col-sm-4">
																		<p class="text-muted text-small mb-1">Cosent Sub Category :</p>
																		<p>
																			<select class="form-control select2-normal" data-width="100%">
																				<option value="0" selected>การให้ยินยอมให้เปิดเผยข้อมูลเฉพาะบางกรณีในกลุ่มธุรกิจการเงิน</option>
																				<option value="1">option1</option>
																				<option value="2">option2</option>
																			</select>
																		</p>
																	</div>
																</div>
																<div class="row mb-0">
																	<div class="col">
																		<p class="text-muted text-small mb-1">Purpose of Consent :</p>
																		<p>
																			<textarea class="form-control rounded-1" rows="4" name="purpose" required=""></textarea>
																			
																		</p>
																	</div>
																	
																</div>
																
																<div class="row mb-0">
																	<div class="col-sm-4">
																		<p class="text-muted text-small mb-1">Consent Start Date:</p>
																		<div class="input-group date">
																			<input type="text" class="form-control" placeholder="dd/mm/yyyy">
																			<span class="input-group-text input-group-append input-group-addon">
																				<i class="simple-icon-calendar"></i>
																			</span>
																		</div>
																	</div>
																	<div class="col-sm-4">
																		<p class="text-muted text-small mb-1">Consent Expire Date:</p>
																		<div class="input-group date">
																			<input type="text" class="form-control" placeholder="dd/mm/yyyy">
																			<span class="input-group-text input-group-append input-group-addon">
																				<i class="simple-icon-calendar"></i>
																			</span>
																		</div>
																	</div>
																	<div class="col-sm-4">
																		<p class="text-muted text-small mb-1">Consent Version:</p>
																		<p>
																			<select class="form-control select2-normal" data-width="100%">
																				<option value="0" selected>1.0</option>
																				<option value="1">option1</option>
																				<option value="2">option2</option>
																			</select>
																		</p>
																	</div>
																</div>
																<div class="row mb-0">
																	<div class="col-sm-4">
																		<p class="text-muted text-small mb-1">Other Document File  :</p>
																		<div class="wrap-drop mb-2 border-dash">
																			<form action="/file-upload">
																				<div class="dropzone">
																				</div>
																			</form>
																		</div>
																		<p class="mb-0 d-flex justify-content-between">
																			<a href="#" class="text-primary">สำเนาบัตรประชาชน.pdf</a>
																			<span><i class="glyph-icon simple-icon-check text-success"></i><i class="glyph-icon simple-icon-close"></i></span>
																		</p>
																		<p class="mb-0 d-flex justify-content-between">
																			<a href="#" class="text-primary">สำเนาทะเบียนบ้าน.pdf</a>
																			<span><i class="glyph-icon simple-icon-check text-success"></i><i class="glyph-icon simple-icon-close"></i></span>
																		</p>
																		<p class="mb-0 d-flex justify-content-between">
																			<a href="#" class="text-primary">สำเนาบุ๊คแบงค์ KTB.pdf</a>
																			<span><i class="glyph-icon simple-icon-check text-success"></i><i class="glyph-icon simple-icon-close"></i></span>
																		</p>
																	</div>
																	<div class="col-sm-4">
																		<p class="text-muted text-small mb-1">Consent file :</p>
																		<div class="wrap-drop mb-2 border-dash">
																			<form action="/file-upload">
																				<div class="dropzone">
																				</div>
																			</form>
																		</div>
																		<p class="mb-0 d-flex justify-content-between">
																			<a href="#" class="text-primary">MBK_2020_loan_agreement.pdf</a>
																			<span><i class="glyph-icon simple-icon-check text-success"></i><i class="glyph-icon simple-icon-close"></i></span>
																		</p>
																		
																		<!--<p><button class="btn btn-outline-primary btn-sm" type="button">Resend  E-mail </button></p>-->
																	</div>
																	<div class="col-sm-4">
																		&nbsp;
																	</div>
																</div>
																
															</div>
															
															<div class="separator mb-3"></div>
															<div class="ctrl-btn d-flex justify-content-end">
																<button class="btn btn-primary mr-2 btn-sm" type="button">Submit</button>
																<button class="btn btn-outline-primary btn-sm" type="button">Cancel</button>
															</div>
															
													   </div>
													   
                                                    </div>
                                                </div>
												  </div>
												  <div class="for-t2 hid">
												   <div class="contract-collapse collapse show" id="c1">
                                                    <div class="card-body pt-0 pl-4 pr-4">
														<div class="d-flex align-items-center mb-4">
															<b>Contact  Name :  </b> 
															<div class="col-sm-5 col-lg-3">
																<select class="form-control select2-normal" data-width="100%">
																	<option value="0" selected>หนังสือให้ความยินยอมเปิดเผยข้อมูล (1) </option>
																	<option value="1">option1</option>
																	<option value="2">option2</option>
																</select>
															</div>
														</div>
													   
													   <div class="detail-card">
													   		<div class="bg-white">
																<div class="row mb-0">
																	<div class="col-sm-4">
																		<p class="text-muted text-small mb-1">System Name :</p>
																		<p>
																			<select class="form-control select2-normal" data-width="100%">
																				<option value="0" selected>MBK Loan system management</option>
																				<option value="1">option1</option>
																				<option value="2">option2</option>
																			</select>
																		</p>
																	</div>
																	<div class="col-sm-4">
																		<p class="text-muted text-small mb-1">Customer Type :</p>
																		<p>
																			<select class="form-control select2-normal" data-width="100%">
																				<option value="0" selected>Customer</option>
																				<option value="1">option1</option>
																				<option value="2">option2</option>
																			</select>
																		</p>
																	</div>
																	<div class="col-sm-4">
																		<p class="text-muted text-small mb-1">Channel Input :</p>
																		<p>
																			<select class="form-control select2-normal" data-width="100%">
																				<option value="0" selected>Call API</option>
																				<option value="1">option1</option>
																				<option value="2">option2</option>
																			</select>
																		</p>
																	</div>
																</div>
															</div>
															
															<div class="bg-white mb-4">
																<div class="row mb-0">
																	<div class="col-sm-4">
																		<p class="text-muted text-small mb-1">Cosent ID :</p>
																		<p>
																			<select class="form-control select2-normal" data-width="100%">
																				<option value="0" selected>1009654</option>
																				<option value="1">option1</option>
																				<option value="2">option2</option>
																			</select>
																		</p>
																	</div>
																	<div class="col-sm-4">
																		<p class="text-muted text-small mb-1">Consent Category  :</p>
																		<p>
																			<select class="form-control select2-normal" data-width="100%">
																				<option value="0" selected>หนังสือให้ความยินยอมเปิดเผยข้อมูล</option>
																				<option value="1">option1</option>
																				<option value="2">option2</option>
																			</select>
																		</p>
																	</div>
																	<div class="col-sm-4">
																		<p class="text-muted text-small mb-1">Cosent Sub Category :</p>
																		<p>
																			<select class="form-control select2-normal" data-width="100%">
																				<option value="0" selected>การให้ยินยอมให้เปิดเผยข้อมูลเฉพาะบางกรณีในกลุ่มธุรกิจการเงิน</option>
																				<option value="1">option1</option>
																				<option value="2">option2</option>
																			</select>
																		</p>
																	</div>
																</div>
																<div class="row mb-0">
																	<div class="col">
																		<p class="text-muted text-small mb-1">Purpose of Consent :</p>
																		<p>
																			<textarea class="form-control rounded-1" rows="4" name="purpose" required=""></textarea>
																			
																		</p>
																	</div>
																	
																</div>
																
																<div class="row mb-0">
																	<div class="col-sm-4">
																		<p class="text-muted text-small mb-1">Consent Start Date:</p>
																		<div class="input-group date">
																			<input type="text" class="form-control" placeholder="dd/mm/yyyy">
																			<span class="input-group-text input-group-append input-group-addon">
																				<i class="simple-icon-calendar"></i>
																			</span>
																		</div>
																	</div>
																	<div class="col-sm-4">
																		<p class="text-muted text-small mb-1">Consent Expire Date:</p>
																		<div class="input-group date">
																			<input type="text" class="form-control" placeholder="dd/mm/yyyy">
																			<span class="input-group-text input-group-append input-group-addon">
																				<i class="simple-icon-calendar"></i>
																			</span>
																		</div>
																	</div>
																	<div class="col-sm-4">
																		<p class="text-muted text-small mb-1">Consent Version:</p>
																		<p>
																			<select class="form-control select2-normal" data-width="100%">
																				<option value="0" selected>1.0</option>
																				<option value="1">option1</option>
																				<option value="2">option2</option>
																			</select>
																		</p>
																	</div>
																</div>
																<div class="row mb-0">
																	<div class="col-sm-4">
																		<p class="text-muted text-small mb-1">Other Document File  :</p>
																		<div class="wrap-drop mb-2 border-dash">
																			<form action="/file-upload">
																				<div class="dropzone">
																				</div>
																			</form>
																		</div>
																		<p class="mb-0 d-flex justify-content-between">
																			<a href="#" class="text-primary">สำเนาบัตรประชาชน.pdf</a>
																			<span><i class="glyph-icon simple-icon-check text-success"></i><i class="glyph-icon simple-icon-close"></i></span>
																		</p>
																		<p class="mb-0 d-flex justify-content-between">
																			<a href="#" class="text-primary">สำเนาทะเบียนบ้าน.pdf</a>
																			<span><i class="glyph-icon simple-icon-check text-success"></i><i class="glyph-icon simple-icon-close"></i></span>
																		</p>
																		<p class="mb-0 d-flex justify-content-between">
																			<a href="#" class="text-primary">สำเนาบุ๊คแบงค์ KTB.pdf</a>
																			<span><i class="glyph-icon simple-icon-check text-success"></i><i class="glyph-icon simple-icon-close"></i></span>
																		</p>
																	</div>
																	<div class="col-sm-4">
																		<p class="text-muted text-small mb-1">Consent file :</p>
																		<div class="wrap-drop mb-2 border-dash">
																			<form action="/file-upload">
																				<div class="dropzone">
																				</div>
																			</form>
																		</div>
																		<p class="mb-0 d-flex justify-content-between">
																			<a href="#" class="text-primary">MBK_2020_loan_agreement.pdf</a>
																			<span><i class="glyph-icon simple-icon-check text-success"></i><i class="glyph-icon simple-icon-close"></i></span>
																		</p>
																		
																		<!--<p><button class="btn btn-outline-primary btn-sm" type="button">Resend  E-mail </button></p>-->
																	</div>
																	<div class="col-sm-4">
																		&nbsp;
																	</div>
																</div>
																
															</div>
															
															<div class="separator mb-3"></div>
															<div class="ctrl-btn d-flex justify-content-end">
																<button class="btn btn-primary mr-2 btn-sm" type="button">Submit</button>
																<button class="btn btn-outline-primary btn-sm" type="button">Cancel</button>
															</div>
															
													   </div>
													   
                                                    </div>
                                                </div>
												  </div>

                                                
                                            </div>
					</div>

					<!-- /tabs-zone -->
					
                </div>
				
				<!-- Modal -->

            </div>
        </div>

    </main>

    

    <?php include("incs/js.html") ?>
	<script src="js/vendor/dropzone.min.js"></script>
    <script src="js/vendor/bootstrap-tagsinput.min.js"></script>
	<script src="js/vendor/datatables.min.js"></script>
    <script src="js/dore.script.js"></script>
    <script src="js/scripts.js"></script>
	<script>
/*	$(document).ready(function() {
    $('#tablelist').DataTable( {
        "paging":   false,
        "ordering": false,
        "info":     false
    } );
} );*/
	//Dropdown plugin data
$(document).ready(function() {
	$('.select2-normal').select2({
  		//placeholder: 'Content Language',
		minimumResultsForSearch: -1,
		//width: 150
	});
	$("[name=type_add]").on("click", function() {
		var isCase = this.value;
		if(isCase == 1) {
			$(".for-t2").removeClass('hid');
			$(".for-t1").addClass('hid');
			//$(".for-t1 .offsrc").prop("disabled",true);
			//$(".for-t2 .offsrc").prop("disabled",false);
		} else {
			$(".for-t2").addClass('hid');
			$(".for-t1").removeClass('hid');
			//$(".for-t2 .offsrc").prop("disabled",true);
			//$(".for-t1 .offsrc").prop("disabled",false);
		}
	  /*console.log(this.value)
	  $('#reagent_code').toggle(this.value == 1)*/
	})
	$("[name=regis_who]:checked").click()

});
</script>
</body>

</html>
<!DOCTYPE html>
<html lang="en">

<!-- Top Head -->
<?php include("incs/header-top.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-sub-hidden show-spinner">
    <?php include("incs/header.html") ?>
    <?php include("incs/sidebar-left.html") ?>

    <main>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="mb-2">
                        <h1>Consent Setting</h1>
						<nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
							<ol class="breadcrumb pt-0">
								<!--<li class="breadcrumb-item">
									<a href="#">Home</a>
								</li>-->
								<li class="breadcrumb-item">
									<a href="#">Consent Category Management </a> |  
								</li>
								<li class="breadcrumb-item active text-gray" aria-current="page">Add Consent Category </li>
							</ol>
						</nav>

                    </div>



                    <div class="mb-3"></div>
					
				
					<div class="d-flex justify-content-center mb-4">

						
					
					<div class="col-sm-10 col-md-8 col-lg-6">
					<div class="card h-100 d-flex justify-content-center flex-row">
					<div class="card-body col-md-9 col-lg-8">

							<div class="wr mb-4">
								<label>Consent Subcategory :</label>
								<input type="text" class="form-control" placeholder="Input consent category">
							</div>
							
	
							
							<div class="wr mb-4">
								<label>Status :</label>
								<select class="form-control select2-single" data-width="100%">
									<option value="0" selected>Active</option>
									<option value="1">Disable</option>
								</select>
							</div>
														
		
														
						<div class="d-flex justify-content-between mb-4">
							<button type="button" class="btn btn-primary btn-sm mb-2 min-w">
								
								Submit</button>
								
								<button type="button" class="btn btn-gray btn-sm mb-2 min-w">
								
								Reset</button>
						</div>
					</div>
					</div>
					</div>
					</div>
					
					
					
					
					
                </div>
            </div>
        </div>

    </main>

    

    <?php include("incs/js.html") ?>
    <script src="js/vendor/bootstrap-tagsinput.min.js"></script>
	
    <script src="js/dore.script.js"></script>
    <script src="js/scripts.js"></script>
	<script>
/*	$(document).ready(function() {
    $('#tablelist').DataTable( {
        "paging":   false,
        "ordering": false,
        "info":     false
    } );
} );*/
	</script>
<script>
$(document).ready(function() {
	$('.main-menu .list-unstyled>li').removeClass('active');
	$('.main-menu .list-unstyled>li:nth-child(3)').addClass('active');
});
</script>
</body>

</html>
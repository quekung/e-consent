<!DOCTYPE html>
<html lang="en">

<!-- Top Head -->
<?php include("incs/header-top.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-sub-hidden show-spinner">
    <?php include("incs/header.html") ?>
    <?php include("incs/sidebar-left.html") ?>

    <main>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="mb-2">
                        <h1>Consent Person</h1>
						<nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
							<ol class="breadcrumb pt-0">
								<!--<li class="breadcrumb-item">
									<a href="#">Home</a>
								</li>
								<li class="breadcrumb-item">
									<a href="#">Library</a>
								</li>-->
								<li class="breadcrumb-item active text-gray" aria-current="page">Consent Person List</li>
							</ol>
						</nav>
                        <!--<div class="top-right-button-container">

                            <div class="btn-group">
                                <div class="btn btn-primary btn-lg pl-4 pr-0 check-button">
                                    <label class="custom-control custom-checkbox mb-0 d-inline-block">
                                        <input type="checkbox" class="custom-control-input" id="checkAll">
                                        <span class="custom-control-label">&nbsp;</span>
                                    </label>
                                </div>
                                <button type="button"
                                    class="btn btn-lg btn-primary dropdown-toggle dropdown-toggle-split"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a class="dropdown-item" href="#">Action</a>
                                    <a class="dropdown-item" href="#">Another action</a>
                                </div>
                            </div>
                        </div>-->
                    </div>

                    <div class="mb-2 d-flex justify-content-between">
						<div class="col-l">
							<a class="btn pt-0 pl-0 d-inline-block d-md-none" data-toggle="collapse" href="#displayOptions"
								role="button" aria-expanded="true" aria-controls="displayOptions">
								Display Options
								<i class="simple-icon-arrow-down align-middle"></i>
							</a>
							<div class="collapse d-md-block" id="displayOptions">
								<div class="d-block d-md-inline-block">
									<div class="float-md-left mr-1 mb-1 dropdown-as-select">
										<label class="d-block">Chanel :</label>
										<button class="btn btn-outline-dark btn-xs dropdown-toggle" type="button"
											data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											All Chanel
										</button>
										<div class="dropdown-menu">
											<a class="dropdown-item active" href="#">All Chanel</a>
											<a class="dropdown-item" href="#">Another action</a>
										</div>
									</div>

									<div class="float-md-left mr-1 mb-1 dropdown-as-select">
										<label class="d-block">Application Service :</label>
										<button class="btn btn-outline-dark btn-xs dropdown-toggle" type="button"
											data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											All Application Services
										</button>
										<div class="dropdown-menu">
											<a class="dropdown-item active" href="#">All Application Services</a>
											<a class="dropdown-item" href="#">Another action</a>
										</div>
									</div>

									<div class="float-md-left mr-1 mb-1 dropdown-as-select">
										<label class="d-block">Type of Person :</label>
										<button class="btn btn-outline-dark btn-xs dropdown-toggle" type="button"
											data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											All Type of person
										</button>
										<div class="dropdown-menu">
											<a class="dropdown-item active" href="#">All Type of person</a>
											<a class="dropdown-item" href="#">Another action</a>
										</div>
									</div>

								</div>
							</div>
						</div>
						<div class="col-r pb-2 top-right-button-container d-flex align-items-end">
                            <!--<button type="button" class="btn btn-primary btn-md top-right-button  mr-1"
                                data-toggle="modal" data-backdrop="static" data-target="#exampleModal">+ Add</button>-->
								<a class="btn btn-primary btn-md top-right-button  mr-1" href="add-dataconsent.php"> + Add</a>
								<!-- add modal -->
                            <div class="modal fade modal-right" id="exampleModal" tabindex="-1" role="dialog"
                                aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Add New</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">

                                            <form>
												<div class="form-group">
                                                    <label>Avatar</label>
													<div class="input-group">
														<div class="input-group-prepend">
															<span class="input-group-text">Upload</span>
														</div>
														<div class="custom-file">
															<input type="file" class="custom-file-input" id="inputGroupFile01">
															<label class="custom-file-label" for="inputGroupFile01">Choose file</label>
														</div>
													</div>
												</div>
                                                <div class="form-group">
                                                    <label>ชื่อ-สกุล (ภาษไทย)</label>
                                                    <input type="text" class="form-control" placeholder="">
                                                </div>
												<div class="form-group">
                                                    <label>Name Surname (English)</label>
                                                    <input type="text" class="form-control" placeholder="">
                                                </div>
												<div class="form-group">
                                                    <label>E-mail</label>
                                                    <input type="text" class="form-control" placeholder="">
                                                </div>
												<div class="form-group">
                                                    <label>Photo Number</label>
                                                    <input type="text" class="form-control" placeholder="">
                                                </div>
												<div class="form-group">
                                                    <label>ID Card</label>
                                                    <input type="text" class="form-control" placeholder="">
                                                </div>
												<div class="form-group">
													<label class="d-block">Gendor</label>
													<div class="btn-group btn-group-toggle" data-toggle="buttons">
														<label class="btn btn-primary active">
															<input type="radio" name="options" id="option1" checked=""> Male
														</label>
														<label class="btn btn-primary">
															<input type="radio" name="options" id="option2"> Female
														</label>
														<label class="btn btn-primary">
															<input type="radio" name="options" id="option3"> Other
														</label>
													</div>
                                                </div>
												<div class="form-group">
                                                    <label>Details</label>
                                                    <textarea class="form-control" rows="2"></textarea>
                                                </div>

                                                <div class="form-group">
                                                    <label>Category</label>
                                                    <select class="form-control select2-single" data-width="100%">
                                                        <option label="&nbsp;">&nbsp;</option>
                                                        <option value="Flexbox">Flexbox</option>
                                                        <option value="Sass">Sass</option>
                                                        <option value="React">React</option>
                                                    </select>
                                                </div>

                                                <div class="form-group">
                                                    <label>Labels</label>
                                                    <select class="form-control select2-multiple" multiple="multiple" data-width="100%">
                                                        <option value="New Framework">New Framework</option>
                                                        <option value="Education">Education</option>
                                                        <option value="Personal">Personal</option>
                                                    </select>
                                                </div>


                                                <div class="form-group">
                                                    <label>Status</label>
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input"
                                                            id="customCheck1">
                                                        <label class="custom-control-label"
                                                            for="customCheck1">Completed</label>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-outline-primary"
                                                data-dismiss="modal">Cancel</button>
                                            <button type="button" class="btn btn-primary">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
							<!-- /add modal -->
						</div>
					</div>

                    <div class="separator mb-5"></div>
					<div class="card">
					<div class="card-body">
						<div id="DataTables_Table_0_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
							<!--<table id="tablelist" class="data-table data-table-feature">-->
							<!--<table id="tablelist" class="data-table data-table-standard">-->
                           <table class="data-table data-tables-pagination responsive nowrap" data-order="[[ 1, &quot;desc&quot; ]]">
							
									<thead>
										<tr>
											<th>No.</th>
											<th>Name Lastname</th>
											<th>E-mail</th>
											<th>Phone Number</th>
											<th>ID Card</th>
											<th class="sort-none">&nbsp;</th>
										</tr>
									</thead>
									<tbody>
										<?php for($i=1;$i<=30;$i++){ ?>
										<tr>
											<td><?php echo $i ?></td>
											<td>
												<a class="p-0 d-flex" href="#">
													<span class="mr-2">
														<img alt="Profile Picture" src="img/profile-pic-l.jpg" height="40" class="rounded-circle">
													</span>
													<span class="name text-left">
														เอสเธอร์ อเล็กซานเดอร์
														<small class="d-block">Esther Alexander</small>
													</span>
													
												</a>
											</td>
											<td><a href="mailto:lillie.foster@example.com">lillie.foster@example.com</a></td>
											<td>302.555.0107</td>
											<td>6783776898788</td>
											<td><a href="person-profile.php" class="btn btn-primary btn-sm btn-block">View</a></td>
										</tr>
										<?php } ?>
										<!-- modal -->
										<div class="modal fade" id="viewProfile" tabindex="-1" role="dialog" style="display: none;" aria-hidden="true">
										<div class="modal-dialog modal-dialog-centered" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<h5 class="modal-title">Profile</h5>
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
														<span aria-hidden="true">×</span>
													</button>
												</div>
												<div class="modal-body">
													<ul class="list-inline">
														<li class="mb-4">
															<a class="p-0 d-flex align-items-center text-dark" href="#">
																<span class="mr-2">
																	<img alt="Profile Picture" src="img/profile-pic-l.jpg" class="rounded-circle" height="100">
																</span>
																<h2 class="name text-left">
																	เอสเธอร์ อเล็กซานเดอร์
																	<small class="d-block">Esther Alexander</small>
																</h2>

															</a>
														</li>
														<li>
															<div class="form-group">
																<label>Email</label>
																<input type="text" class="form-control" placeholder="" value="lillie.foster@example.com">
															</div>
														</li>
														<li>
															<div class="form-group">
																<label>Mobile Number</label>
																<input type="text" class="form-control" placeholder="" value="302.555.0107">
															</div>
														</li>
														<li>
															<div class="form-group">
																<label>ID Card</label>
																<input type="text" class="form-control" placeholder="" value=" 	6783776898788">
															</div>
														</li>
														
													</ul>

												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
													<button type="button" class="btn btn-primary">Save changes</button>
												</div>
											</div>
										</div>
									</div>
									<!-- /modal -->

									</tbody>
								</table>
								
		

						</div>
					</div>
					</div>
                </div>
            </div>
        </div>

    </main>

    

    <?php include("incs/js.html") ?>
	
	 <script src="js/vendor/datatables.min.js"></script>
	 
    <script src="js/dore.script.js"></script>
    <script src="js/scripts.js"></script>
	<script>
/*	$(document).ready(function() {
    $('#tablelist').DataTable( {
        "paging":   false,
        "ordering": false,
        "info":     false
    } );
} );*/
	</script>
</body>

</html>
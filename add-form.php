<!DOCTYPE html>
<html lang="en">

<!-- Top Head -->
<?php include("incs/header-top.html") ?>
<!-- /Top Head -->

<body id="app-container" class="menu-sub-hidden show-spinner">
    <?php include("incs/header.html") ?>
    <?php include("incs/sidebar-left.html") ?>

    <main>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="mb-2">
                        <h1>ADD NEW</h1>
						<nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
							<ol class="breadcrumb pt-0">
								<!--<li class="breadcrumb-item">
									<a href="#">Home</a>
								</li>
								<li class="breadcrumb-item">
									<a href="#">Library</a>
								</li>-->
								<li class="breadcrumb-item active text-gray" aria-current="page">Consent Person</li>
							</ol>
						</nav>
                        <!--<div class="top-right-button-container">

                            <div class="btn-group">
                                <div class="btn btn-primary btn-lg pl-4 pr-0 check-button">
                                    <label class="custom-control custom-checkbox mb-0 d-inline-block">
                                        <input type="checkbox" class="custom-control-input" id="checkAll">
                                        <span class="custom-control-label">&nbsp;</span>
                                    </label>
                                </div>
                                <button type="button"
                                    class="btn btn-lg btn-primary dropdown-toggle dropdown-toggle-split"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a class="dropdown-item" href="#">Action</a>
                                    <a class="dropdown-item" href="#">Another action</a>
                                </div>
                            </div>
                        </div>-->
                    </div>



                    <div class="separator mb-5"></div>
					
					<div class="card mb-4">
					<div class="card-body">
						<h5 class="mb-4">Input</h5>
						<form>
												<!--<div class="form-group">
                                                    <label>Avatar</label>
													<div class="input-group">
														<div class="input-group-prepend">
															<span class="input-group-text">Upload</span>
														</div>
														<div class="custom-file">
															<input type="file" class="custom-file-input" id="inputGroupFile01">
															<label class="custom-file-label" for="inputGroupFile01">Choose file</label>
														</div>
													</div>
												</div>-->
												<div class="row">
                                                <div class="col-sm-6 form-group">
                                                    <label>ชื่อ-สกุล (ภาษไทย)</label>
                                                    <input type="text" class="form-control" placeholder="">
                                                </div>
												<div class="col-sm-6 form-group">
                                                    <label>Name Surname (English)</label>
                                                    <input type="text" class="form-control" placeholder="">
                                                </div>
												</div>
												<div class="row">
												<div class="col-sm-4 form-group">
                                                    <label>E-mail</label>
                                                    <input type="text" class="form-control" placeholder="">
                                                </div>
												<div class="col-sm-4 form-group">
                                                    <label>Photo Number</label>
                                                    <input type="text" class="form-control" placeholder="">
                                                </div>
												<div class="col-sm-4 form-group">
                                                    <label>ID Card</label>
                                                    <input type="text" class="form-control" placeholder="">
                                                </div>
												
												</div>
												
												<div class="form-group mb-3">
													<label>Date</label>
													<input class="form-control datepicker" placeholder="Date">
												</div>
												
												<div class="input-daterange d-flex flex-nowrap align-items-end" id="datepicker"> 
													<div class="form-group col">
														<label>Start date :</label>
														<input type="text" class="form-control bg-light rounded-2" name="Select date"
																	placeholder="Start" />
													</div>
													<span class="form-group pl-2 pr-2"><label>to</label></span>
													<div class="form-group col">
														<label>End date :</label>
														<input type="text" class="form-control bg-light rounded-2" name="Select date"
																	placeholder="End" />
													</div>
												</div>
												
												<div class="form-group">
													<label class="d-block">Gendor</label>
													<div class="btn-group btn-group-toggle" data-toggle="buttons">
														<label class="btn btn-primary active">
															<input type="radio" name="options" id="option1" checked=""> Male
														</label>
														<label class="btn btn-primary">
															<input type="radio" name="options" id="option2"> Female
														</label>
														<label class="btn btn-primary">
															<input type="radio" name="options" id="option3"> Other
														</label>
													</div>
                                                </div>
												<div class="form-group">
                                                    <label>Details</label>
                                                    <textarea class="form-control" rows="2"></textarea>
                                                </div>

                                                <div class="form-group">
                                                    <label>Category</label>
                                                    <select class="form-control select2-single select2-hidden-accessible" data-width="100%" tabindex="-1" aria-hidden="true">
                                                        <option label="&nbsp;">&nbsp;</option>
                                                        <option value="Flexbox">Flexbox</option>
                                                        <option value="Sass">Sass</option>
                                                        <option value="React">React</option>
                                                    </select>
                                                </div>

                                                <div class="form-group">
                                                    <label>Labels</label>
                                                    <select class="form-control select2-multiple select2-hidden-accessible" multiple="" data-width="100%" tabindex="-1" aria-hidden="true">
                                                        <option value="New Framework">New Framework</option>
                                                        <option value="Education">Education</option>
                                                        <option value="Personal">Personal</option>
                                                    </select>
                                                </div>


                                                <div class="form-group">
                                                    <label>Status</label>
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="customCheck1">
                                                        <label class="custom-control-label" for="customCheck1">Completed</label>
                                                    </div>
                                                </div>
                                            </form>
					</div>
					</div>
					
					<div class="row mb-4">
					<div class="col-sm-6">
						<div class="card h-100">
						<div class="card-body">
							<h5 class="mb-4">checkbox</h5>
								<p class="text-muted text-small">Labels</p>
								<div class="form-group">
								<p class="d-sm-inline-block mb-1">
									<a href="#">
										<span class="badge badge-pill badge-outline-primary mb-1">NEW FRAMEWORK</span>
									</a>
								</p>

								<p class="d-sm-inline-block mb-1">
									<a href="#">
										<span class="badge badge-pill badge-outline-theme-3 mb-1">EDUCATION</span>
									</a>
								</p>
								<p class="d-sm-inline-block  mb-1">
									<a href="#">
										<span class="badge badge-pill badge-outline-secondary mb-1">PERSONAL</span>
									</a>
								</p>
							</div>
							<div class="body">
							<h5 class="mb-2">Button</h5>
								<button type="button" class="btn btn-primary mb-1">Primary</button>
                            <button type="button" class="btn btn-secondary mb-1">Secondary</button>
                            <button type="button" class="btn btn-success mb-1">Success</button>
                            <button type="button" class="btn btn-danger mb-1">Danger</button>
                            <button type="button" class="btn btn-warning mb-1">Warning</button>
                            <button type="button" class="btn btn-info mb-1">Info</button>
                            <button type="button" class="btn btn-light mb-1">Light</button>
                            <button type="button" class="btn btn-dark mb-1">Dark</button>
							</div>
							
							<div class="body ">
                            <h5 class="mb-2">Outline</h5>
                            <button type="button" class="btn btn-outline-primary mb-1">Primary</button>
                            <button type="button" class="btn btn-outline-secondary mb-1">Secondary</button>
                            <button type="button" class="btn btn-outline-success mb-1">Success</button>
                            <button type="button" class="btn btn-outline-danger mb-1">Danger</button>
                            <button type="button" class="btn btn-outline-warning mb-1">Warning</button>
                            <button type="button" class="btn btn-outline-info mb-1">Info</button>
                            <button type="button" class="btn btn-outline-light mb-1">Light</button>
                            <button type="button" class="btn btn-outline-dark mb-1">Dark</button>
                        </div>
						
							<div class="body ">
                            <h5 class="mb-2">Sizes</h5>

                            <div class="mb-2">
                                <h6 class="mb-2">Large Button</h6>
                                <button type="button" class="btn btn-primary btn-lg mb-1">LARGE BUTTON</button>
                                <button type="button" class="btn btn-secondary btn-lg mb-1">LARGE BUTTON</button>
                            </div>
                            <div class="mb-2">
                                <h6 class="mb-2">Small Button</h6>
                                <button type="button" class="btn btn-primary btn-sm mb-1">Small Button</button>
                                <button type="button" class="btn btn-secondary btn-sm mb-1">Small Button</button>
                            </div>
                            <div class="mb-2">
                                <h6 class="mb-2">Extra Small Button</h6>
                                <button type="button" class="btn btn-primary btn-xs mb-1">Xs Button</button>
                                <button type="button" class="btn btn-secondary btn-xs mb-1">Xs Button</button>
                            </div>
                            <div>
                                <h6 class="mb-2">Block Button</h6>
                                <button type="button" class="btn btn-primary btn-block mb-1">Block level button</button>
                                <button type="button" class="btn btn-secondary btn-block mb-1">Block level
                                    button</button>
                            </div>
                        </div>
							
							<div class="text-right mb-4">
								<button type="button" class="btn btn-primary btn-sm mb-2">
									<i class="glyph-icon iconsminds-save"></i>
									Save</button>
							</div>
						</div>
						</div>
					</div>
						
					
					<div class="col-sm-6">
					<div class="card h-100">
					<div class="card-body">
						<h5 class="mb-4">Select / radio / checkbox</h5>
						
						<div class="view-mode mb-4">
                                                            <label>How old are you?</label>
                                                            <select class="form-control select2-single" data-width="100%">
                                                                <option label="&nbsp;">&nbsp;</option>
                                                                <option value="0">18-24</option>
                                                                <option value="1">24-30</option>
                                                                <option value="2">30-40</option>
                                                                <option value="3">40-50</option>
                                                                <option value="4">50+</option>
                                                            </select>
                                                        </div>
														
														<div class="view-mode">
                                                            <label>What is your gender?</label>
                                                            <div class="mb-4">
                                                                <div class="custom-control custom-radio">
                                                                    <input type="radio" id="customRadio1"
                                                                        name="customRadio" class="custom-control-input">
                                                                    <label class="custom-control-label"
                                                                        for="customRadio1">Male</label>
                                                                </div>
                                                                <div class="custom-control custom-radio">
                                                                    <input type="radio" id="customRadio2"
                                                                        name="customRadio" class="custom-control-input">
                                                                    <label class="custom-control-label"
                                                                        for="customRadio2">Female</label>
                                                                </div>
                                                                <div class="custom-control custom-radio">
                                                                    <input type="radio" id="customRadio3"
                                                                        name="customRadio" class="custom-control-input">
                                                                    <label class="custom-control-label"
                                                                        for="customRadio3">Other</label>
                                                                </div>
                                                            </div>
                                                        </div>
														
														<div class="view-mode">
                                                            <label>What programming languages do you use?</label>
                                                            <div class="mb-4">
                                                                <div class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input"
                                                                        id="lang1">
                                                                    <label class="custom-control-label"
                                                                        for="lang1">Python</label>
                                                                </div>
                                                                <div class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input"
                                                                        id="lang2">
                                                                    <label class="custom-control-label"
                                                                        for="lang2">JavaScript</label>
                                                                </div>
                                                                <div class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input"
                                                                        id="lang3">
                                                                    <label class="custom-control-label"
                                                                        for="lang3">PHP</label>
                                                                </div>
                                                                <div class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input"
                                                                        id="lang4">
                                                                    <label class="custom-control-label"
                                                                        for="lang4">Java</label>
                                                                </div>
                                                                <div class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input"
                                                                        id="lang5">
                                                                    <label class="custom-control-label"
                                                                        for="lang5">C#</label>
                                                                </div>
                                                            </div>
                                                        </div>
														
														<div class="d-flex justify-content-between mb-4">
							<button type="button" class="btn btn-info btn-sm mb-2">
								
								Edit</button>
								
								<button type="button" class="btn btn-light btn-sm mb-2">
								
								close</button>
						</div>
					</div>
					</div>
					</div>
					</div>
					
					<div class="card mb-4">
                        <div class="card-body ">
                            <h5 class="mb-4">CKEditor </h5>
                            <textarea name="content" id="ckEditorClassic"></textarea>
                        </div>
                    </div>
					
					<div class="card mb-4">
                        <div class="card-body">
                            <h5 class="mb-4">Basic</h5>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">@</span>
                                </div>
                                <input type="text" class="form-control" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1">
                            </div>

                            <div class="input-group mb-3">
                                <input type="text" class="form-control" placeholder="Recipient's username" aria-label="Recipient's username" aria-describedby="basic-addon2">
                                <div class="input-group-append">
                                    <span class="input-group-text" id="basic-addon2">@example.com</span>
                                </div>
                            </div>

                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon3">https://example.com/users/</span>
                                </div>
                                <input type="text" class="form-control" id="basic-url" aria-describedby="basic-addon3">
                            </div>

                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">$</span>
                                </div>
                                <input type="text" class="form-control" aria-label="Amount (to the nearest dollar)">
                                <div class="input-group-append">
                                    <span class="input-group-text">.00</span>
                                </div>
                            </div>

                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">With textarea</span>
                                </div>
                                <textarea class="form-control" aria-label="With textarea"></textarea>
                            </div>

                        </div>
                    </div>
					
					<div class="card mb-4">
                        <div class="card-body">
                            <h5 class="mb-4">Custom File Input</h5>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Upload</span>
                                </div>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="inputGroupFile01">
                                    <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                                </div>
                            </div>

                            <div class="input-group mb-3">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="inputGroupFile02">
                                    <label class="custom-file-label" for="inputGroupFile02">Choose file</label>
                                </div>
                                <div class="input-group-append">
                                    <span class="input-group-text">Upload</span>
                                </div>
                            </div>

                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <button class="btn btn-outline-secondary" type="button">Button</button>
                                </div>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="inputGroupFile03">
                                    <label class="custom-file-label" for="inputGroupFile03">Choose file</label>
                                </div>
                            </div>

                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="inputGroupFile04">
                                    <label class="custom-file-label" for="inputGroupFile04">Choose file</label>
                                </div>
                                <div class="input-group-append">
                                    <button class="btn btn-outline-secondary" type="button">Button</button>
                                </div>
                            </div>
                        </div>
                    </div>
					
					<div class="text-center mb-4">
						<button type="button" class="btn btn-light btn-sm mb-2 mr-1">Cancel</button>
						<button type="button" class="btn btn-primary btn-sm mb-2 ml-1">Submit</button>
					</div>
					
                </div>
            </div>
        </div>

    </main>

    

    <?php include("incs/js.html") ?>
    <script src="js/vendor/bootstrap-tagsinput.min.js"></script>
	
    <script src="js/dore.script.js"></script>
    <script src="js/scripts.js"></script>
	<script>
/*	$(document).ready(function() {
    $('#tablelist').DataTable( {
        "paging":   false,
        "ordering": false,
        "info":     false
    } );
} );*/
	</script>
</body>

</html>